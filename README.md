# Stratus API Plugins

The suite of plugins which provide access to backend services on [Stratus Network](https://stratus.network) servers.

The suite consists of the core plugins for Bukkit and Velocity, and additional plugins which can be added on top to provide additional server-specific functionality. Backend interaction is largely performed through HTTP requests, with AMQP used to provide asynchronous messaging between servers and notifications from the backend.

## Licensing

These plugins are licensed under the GNU Affero General Public License v3.0. For more information, please see the LICENSE file.

You can also view a [summary of this licence's provisions and requirements](https://choosealicense.com/licenses/agpl-3.0/), but this summary does not constitute legal advice.

## Contributing

This suite of plugins is maintained by the Stratus Network development team. If you are interested in contributing to Stratus development, please contact `@Half#6583` on Discord or email [support@stratus.network](mailto:support@stratus.network).

As this project is open source, you are also welcome to [open a merge request](https://gitlab.com/StratusNetwork/minecraft/stratus-api-plugins/-/merge_requests/new), but we recommend contacting us first to ensure your contribution is not wasted.

## Compiling

First, install [Stratus' PGM](https://github.com/ShinyDialga/PGM/tree/stratus) to maven using `mvn clean install`. Make sure you are on the `stratus` branch of Stratus' PGM (`git checkout stratus`). Then, compile Stratus API Plugins using `mvn clean install` in the main directory.

## Suggesting features

Feature suggestions are not being accepted at this time.

## Reporting issues

If you've found a bug in the plugins, please [open an issue](https://gitlab.com/StratusNetwork/minecraft/stratus-api-plugins/-/issues) or contact `@Half#6583` on Discord. You can also [open a merge request](https://gitlab.com/StratusNetwork/minecraft/stratus-api-plugins/-/merge_requests/new) with the required changes to fix the issue.

If you've found a security vulnerability, please email details to [security@stratus.network](mailto:security@stratus.network) or contact `@Half#6583` on Discord.

## Other resources

Stratus uses the PvP Game Manager (PGM) plugin, which is an open source Bukkit plugin with a thriving development community. For more information, to report issues and to contribute to PGM, please visit the repository on GitHub: [https://github.com/PGMDev/PGM](https://github.com/PGMDev/PGM)

Stratus also maintains a fork of PGM with some small changes: [https://github.com/StratusNetwork/PGM](https://github.com/StratusNetwork/PGM)
