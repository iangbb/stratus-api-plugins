/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Default;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.bukkit.requests.BukkitPunishmentModificationRequest;
import network.stratus.api.bukkit.requests.PunishmentCreateRequest;
import network.stratus.api.bukkit.requests.PunishmentLookupRequest;
import network.stratus.api.bukkit.requests.PunishmentViewRequest;
import network.stratus.api.requests.IpBanCreateRequest;
import network.stratus.api.requests.IpBanDeleteRequest;
import network.stratus.api.requests.IpBanViewRequest;
import network.stratus.api.requests.PunishmentModificationRequest;
import network.stratus.api.responses.IpBanViewResponse;
import network.stratus.api.responses.ObjectIdResponse;
import network.stratus.api.bukkit.responses.PunishmentLookupResponse;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.IpBan;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.PunishmentFactory.Type;
import network.stratus.api.models.punishment.TimedPunishment;

/**
 * Commands for punishing players, viewing punishments and managing existing punishments.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentCommands {
	
	@Command(aliases = { "punish", "p" },
			desc = "Punish a player, automatically determining the punishment type",
			perms = "stratusapi.command.punish",
			usage = "[-s] <player> <reason>")
	public void onPunish(CommandSender sender, TabCompletePlayer target, @Text String reason, @Switch('s') boolean silent) {
		makeRequest(sender, target, reason, Type.AUTO, silent);
	}
	
	@Command(aliases = { "warn", "w" },
			desc = "Warn a player",
			perms = "stratusapi.command.warn",
			usage = "<player> <reason>")
	public void onWarn(CommandSender sender, TabCompletePlayer target, @Text String reason) {
		makeRequest(sender, target, reason, Type.WARN, false);
	}
	
	@Command(aliases = { "kick", "k" },
			desc = "Kick a player",
			perms = "stratusapi.command.kick",
			usage = "[-s] <player> <reason>")
	public void onKick(CommandSender sender, TabCompletePlayer target, @Text String reason, @Switch('s') boolean silent) {
		makeRequest(sender, target, reason, Type.KICK, silent);
	}
	
	@Command(aliases = { "tempban", "tb" },
			desc = "Temporarily ban a player for the specified duration",
			perms = "stratusapi.command.tempban",
			usage = "[-s] <player> <duration> <reason>")
	public void onTempban(CommandSender sender, TabCompletePlayer target, String duration, @Text String reason, @Switch('s') boolean silent) {
		makeRequest(sender, target, reason, Type.BAN, duration, silent);
	}
	
	@Command(aliases = { "permaban", "pb" },
			desc = "Permanently ban a player",
			perms = "stratusapi.command.permaban",
			usage = "[-s] <player> <reason>")
	public void onPermaban(CommandSender sender, TabCompletePlayer target, @Text String reason, @Switch('s') boolean silent) {
		makeRequest(sender, target, reason, Type.BAN, silent);
	}
	
	@Command(aliases = { "lookup" , "l" },
			desc = "Lookup a player's infractions",
			perms = "stratusapi.command.lookup",
			usage = "<player> [<number>]")
	public void onLookup(CommandSender sender, TabCompletePlayer target, @Default("0") int number) {
		SingleAudience audience = new SingleAudience(sender);
		Type[] types;
		if (sender instanceof ConsoleCommandSender || sender.hasPermission("stratusapi.punishments.warn.see.others")
				|| (sender.getName().equalsIgnoreCase(target.getUsername()))) {
			types = new Type[] { Type.WARN, Type.KICK, Type.BAN, Type.MUTE };
		} else {
			types = new Type[] { Type.KICK, Type.BAN, Type.MUTE };
		}

		if (number == 0) {
			// If 0 (default), then look up the list of punishments
			StratusAPI.get().newSharedChain("punishments").<PunishmentLookupResponse>asyncFirst(() -> {
				return new PunishmentLookupRequest(target.getUsername(), types).make(StratusAPI.get().getApiClient());
			}).syncLast(response -> {
				final String targetDisplay = Chat.getDisplayName(response.getTarget());
				List<BukkitPunishmentFactory> list = response.getList();
				if (list.isEmpty()) {
					audience.sendMessage("punishment.lookup.empty", targetDisplay);
				} else {
					audience.sendMessage("punishment.lookup.title", targetDisplay);
					for (BukkitPunishmentFactory bpf : list) {
						Punishment p = bpf.getObject();
						String type = (p.hasExpired() ? ChatColor.STRIKETHROUGH : "")
								+ p.getType().getType().toLowerCase() + ChatColor.RESET;
						audience.sendMessage(
								"punishment.lookup.entry", p.getNumber(), StratusAPI.get().getTranslator()
										.getTimeDifferenceString(sender.getLocale(), p.getTime(), new Date()),
								p.getIssuer().getUsername(), type, p.getReason());
					}
				}
			}).execute((e, task) -> Util.handleCommandException(e, task, sender));
		} else if (number > 0) {
			// If positive, then number provided, so look up details of specific punishment
			StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
				return new PunishmentViewRequest(target.getUsername(), number, types)
						.make(StratusAPI.get().getApiClient());
			}).syncLast(response -> {
				Punishment punishment = response.getObject();
				final String issuerDisplay = Chat.getDisplayName(punishment.getIssuer());
				final String targetDisplay = Chat.getDisplayName(punishment.getTarget());

				final String timeRelative = StratusAPI.get().getTranslator().getTimeDifferenceString(sender.getLocale(),
						punishment.getTime(), new Date());
				final String timeAbsolute = DateFormat
						.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, sender.getLocale())
						.format(punishment.getTime());

				audience.sendMessage("punishment.view.title", targetDisplay);
				audience.sendMessage("punishment.view.id", punishment.get_id());
				audience.sendMessage(
						punishment.isActive() ? "punishment.view.active.yes" : "punishment.view.active.no");
				audience.sendMessage("punishment.view.type", punishment.getType().toString().toLowerCase());
				audience.sendMessage("punishment.view.issuer", issuerDisplay);
				audience.sendMessage("punishment.view.reason", punishment.getReason());
				audience.sendMessage("punishment.view.issuetime", timeRelative, timeAbsolute);
				if (punishment instanceof TimedPunishment) {
					Date now = new Date();
					long remaining = ((TimedPunishment) punishment).getTimeRemaining();
					audience.sendMessage("punishment.view.timeremaining",
							Util.differenceInHours(now, new Date(now.getTime() + remaining)));
				} else if (punishment.getExpiry() == null) {
					audience.sendMessage("punishment.view.expire.never");
				} else {
					final String expiryRelative = StratusAPI.get().getTranslator()
							.getTimeDifferenceString(sender.getLocale(), punishment.getExpiry(), new Date());
					final String expiryAbsolute = DateFormat
							.getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT, sender.getLocale())
							.format(punishment.getExpiry());

					audience.sendMessage(
							(punishment.hasExpired() ? "punishment.view.expire.past" : "punishment.view.expire.future"),
							expiryRelative, expiryAbsolute);
				}
				audience.sendMessage("punishment.view.server", punishment.getServerName());
			}).execute((e, task) -> Util.handleCommandException(e, task, sender));
		} else {
			// Invalid input
			audience.sendMessage("punishment.lookup.invalid.negative");
		}
	}
	
	@Command(aliases = "appeal",
			desc = "Mark a punishment as invalid",
			perms = "stratusapi.command.appeal",
			usage = "<player> <punishment number>")
	public void onAppeal(CommandSender sender, TabCompletePlayer target, int number) {
		PunishmentModificationRequest<BukkitPunishmentFactory> request = new BukkitPunishmentModificationRequest.Builder(
				target.getUsername(), number, (sender instanceof Player) ? ((Player) sender).getUniqueId() : null,
				sender.hasPermission("stratusapi.command.appeal.others")).active(false).build();

		StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			audience.sendMessage("punishment.appeal.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "expire",
			desc = "Expire a punishment immediately or set its new expiry",
			perms = "stratusapi.command.expire",
			usage = "<player> <punishment number> [<new duration>]")
	public void onExpire(CommandSender sender, TabCompletePlayer target, int number, @Default("") String duration) {
		PunishmentModificationRequest<BukkitPunishmentFactory> request = new BukkitPunishmentModificationRequest.Builder(
				target.getUsername(), number, (sender instanceof Player) ? ((Player) sender).getUniqueId() : null,
				sender.hasPermission("stratusapi.command.expire.others")).duration(duration).build();

		StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			audience.sendMessage("punishment.expire.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}
	
	@Command(aliases = "setreason",
			desc = "Set a new reason for the given punishment",
			perms = "stratusapi.command.reason",
			usage = "<player> <punishment number> <new reason>")
	public void onEditReason(CommandSender sender, TabCompletePlayer target, int number, @Text String reason) {
		PunishmentModificationRequest<BukkitPunishmentFactory> request = new BukkitPunishmentModificationRequest.Builder(
				target.getUsername(), number, (sender instanceof Player) ? ((Player) sender).getUniqueId() : null,
				sender.hasPermission("stratusapi.command.reason.others")).reason(reason).build();

		StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			audience.sendMessage("punishment.reason.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "settype",
			desc = "Change the type of a punishment",
			perms = "stratusapi.command.type",
			usage = "<player> <punishment number> <new type>")
	public void onSetType(CommandSender sender, TabCompletePlayer target, int number, String type) {
		PunishmentModificationRequest<BukkitPunishmentFactory> request = new BukkitPunishmentModificationRequest.Builder(
				target.getUsername(), number, (sender instanceof Player) ? ((Player) sender).getUniqueId() : null,
				sender.hasPermission("stratusapi.command.type.others")).type(type).build();

		StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			audience.sendMessage("punishment.type.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "mute", "m" },
			desc = "Mute a player",
			perms = "stratusapi.command.mute",
			usage = "[-t duration] <player> <reason>")
	public void onMute(CommandSender sender, TabCompletePlayer target, @Text String reason, @Switch('t') String duration) {
		makeRequest(sender, target, reason, Type.MUTE, duration, false);
	}

	@Command(aliases = { "ban-ip", "banip", "ip-ban", "ipban" },
			desc = "Ban an IP address",
			perms = "stratusapi.command.ipban",
			usage = "<IP address> [-e <end IP>] <description>")
	public void onIpBan(CommandSender sender, String ip, @Text String description, @Switch('e') String endIp) {
		StratusAPI.get().newSharedChain("ipban").<ObjectIdResponse>asyncFirst(() -> {
			return new IpBanCreateRequest(ip, endIp, description).make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("punishment.ipban.success", response.get_id());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "check-ip", "checkip" },
			desc = "Check an IP address to see if it is banned",
			perms = "stratusapi.command.checkip",
			usage = "<IP address>")
	public void onCheckIpBan(CommandSender sender, String ip) {
		StratusAPI.get().newSharedChain("ipban").<IpBanViewResponse>asyncFirst(() -> {
			return new IpBanViewRequest(ip).make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			SingleAudience audience = new SingleAudience(sender);
			if (response.getIpBans().isEmpty()) {
				audience.sendMessage("punishment.ipban.view.empty");
			} else {
				for (IpBan ipBan : response.getIpBans()) {
					audience.sendMessage("punishment.ipban.view.1", ipBan.getBannedIp());
					audience.sendMessage("punishment.ipban.view.2", ipBan.get_id());
					audience.sendMessage("punishment.ipban.view.3", ipBan.getTime());
					audience.sendMessage("punishment.ipban.view.4", ipBan.getDescription());
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = { "unban-ip", "unbanip" },
			desc = "Remove an IP ban",
			perms = "stratusapi.command.unbanip",
			usage = "<IP ban object ID>")
	public void onUnbanIp(CommandSender sender, String objectId) {
		StratusAPI.get().newSharedChain("ipban").<Void>asyncFirst(() -> {
			return new IpBanDeleteRequest(objectId).make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("punishment.ipban.delete.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	private void makeRequest(CommandSender sender, TabCompletePlayer target, String reason, Type type, boolean silent) {
		makeRequest(sender, target, reason, type, null, silent);
	}

	private void makeRequest(CommandSender sender, TabCompletePlayer target, String reason, Type type, String duration, boolean silent) {
		final User issuer;
		if (sender instanceof Player) {
			issuer = new User(((Player) sender).getUniqueId(), sender.getName());
		} else {
			issuer = User.CONSOLE;
		}

		PunishmentCreateRequest request = new PunishmentCreateRequest(issuer, target.getUsername(), reason, type, duration, silent);
		StratusAPI.get().newSharedChain("punishments").<BukkitPunishmentFactory>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			response.getObject().enforce(false);
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
