/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.server;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import network.stratus.api.bukkit.StratusAPI;

/**
 * Tracks cooldowns for execution of certain operations by players, such as
 * commands, allowing them to only be run at a certain maximum rate.
 * 
 * @author Ian Ballingall
 *
 */
public class CooldownManager implements Listener {

	private final long cooldownTime;
	private Map<UUID, Long> lastExecutionMap;

	public CooldownManager(long cooldownTime) {
		this.cooldownTime = cooldownTime;
		this.lastExecutionMap = new HashMap<>();
		StratusAPI.get().getServer().getPluginManager().registerEvents(this, StratusAPI.get());
	}

	public long getCooldownTime() {
		return cooldownTime;
	}

	/**
	 * Sets the last execution time to the current time.
	 * 
	 * @param uuid The player's {@link UUID}
	 */
	public void setLastExecutionTime(UUID uuid) {
		setLastExecutionTime(uuid, System.currentTimeMillis());
	}

	/**
	 * Sets the last execution time to the given time.
	 * 
	 * @param uuid      The player's {@link UUID}
	 * @param timestamp The time in milliseconds
	 */
	public void setLastExecutionTime(UUID uuid, long timestamp) {
		lastExecutionMap.put(uuid, timestamp);
	}

	/**
	 * Retrieve the last execution time for the given player.
	 * 
	 * @param uuid The player's {@link UUID}
	 * @return The time the operation was last performed in milliseconds
	 */
	public long getLastExecutionTime(UUID uuid) {
		return lastExecutionMap.getOrDefault(uuid, -1L);
	}

	/**
	 * Determine if the operation is allowed or prevented by cooldown.
	 * 
	 * @param uuid The player's {@link UUID}
	 * @return Whether the operation is allowed
	 */
	public boolean isExecutionAllowed(UUID uuid) {
		if (lastExecutionMap.containsKey(uuid)) {
			return lastExecutionMap.get(uuid) + cooldownTime < System.currentTimeMillis();
		} else {
			return true;
		}
	}

	/**
	 * Determine if the operation is allowed or prevented by cooldown. If permitted,
	 * the last execution time is updated, on the assumption the operation is now
	 * executed.
	 * 
	 * @param uuid The player's {@link UUID}
	 * @return Whether the operation is allowed
	 */
	public boolean executeIfAllowed(UUID uuid) {
		if (isExecutionAllowed(uuid)) {
			lastExecutionMap.put(uuid, System.currentTimeMillis());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Called when the player disconnects, removing their cooldown mapping.
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		lastExecutionMap.remove(event.getPlayer().getUniqueId());
	}

}
