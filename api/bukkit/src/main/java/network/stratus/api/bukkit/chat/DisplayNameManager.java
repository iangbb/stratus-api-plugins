/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.chat;

import org.bukkit.entity.Player;

/**
 * Represents a manager for display names. This should be implemented by a class
 * which manages how a {@link Player}'s display name is set.
 * 
 * @author Ian Ballingall
 *
 */
public interface DisplayNameManager {

	/**
	 * Set the given {@link Player}'s display name.
	 * 
	 * @param player The {@link Player} whose display name to set
	 */
	public void setDisplayName(Player player);

}
