/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.io.IOException;

import javax.annotation.Nullable;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import app.ashcon.intake.Command;
import app.ashcon.intake.CommandException;
import app.ashcon.intake.parametric.annotation.Switch;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.messaging.Publisher;
import network.stratus.api.models.AdminChatMessage;
import network.stratus.api.models.GlobalBroadcast;

/**
 * Chat commands, which may be transmitted over a messaging service.
 * 
 * @author Ian Ballingall
 *
 */
public class ChatCommands {

	private @Nullable Publisher adminChatPublisher;
	private @Nullable Publisher broadcastPublisher;

	public ChatCommands(@Nullable Publisher adminChatPublisher, @Nullable Publisher broadcastPublisher) {
		this.adminChatPublisher = adminChatPublisher;
		this.broadcastPublisher = broadcastPublisher;
	}

	@Command(aliases = "a",
			desc = "Send a message to other staff members",
			perms = "stratusapi.adminchat")
	public void onAdminChat(CommandSender sender, @Text String message) {
		Player senderPlayer = (sender instanceof Player) ? (Player) sender : null;
		new MultiAudience.Builder().includePermission("stratusapi.adminchat").build().sendMessage("adminchat.message",
				(senderPlayer == null) ? Chat.OFFLINE_COLOR + "CONSOLE" : senderPlayer.getDisplayName(), message);

		if (adminChatPublisher != null) {
			AdminChatMessage publishMessage = new AdminChatMessage(
					(senderPlayer == null) ? null : senderPlayer.getUniqueId(), sender.getName(),
					StratusAPI.get().getServerName(), message);
			new BukkitRunnable() {
				@Override
				public void run() {
					try {
						adminChatPublisher.publish(publishMessage);
					} catch (IOException e) {
						StratusAPI.get().getLogger().severe("Failed to send admin chat message: " + e);
						e.printStackTrace();
					}
				}
			}.runTaskAsynchronously(StratusAPI.get());
		}
	}

	@Command(aliases = "broadcast",
			desc = "Broadcast a message to the server or the network",
			perms = "stratusapi.command.broadcast",
			usage = "[-g] <message>")
	public void onBroadcast(CommandSender sender, @Text String message, @Switch('g') boolean global)
			throws CommandException {
		new MultiAudience.Builder().global().build().sendMessage("broadcast", message);

		if (global) {
			if (broadcastPublisher == null) {
				new SingleAudience(sender).sendMessage("broadcast.global.disabled");
			} else if (sender.hasPermission("stratusapi.command.broadcast.global")) {
				GlobalBroadcast broadcast = new GlobalBroadcast(message, StratusAPI.get().getServerName());
				new BukkitRunnable() {
					@Override
					public void run() {
						try {
							broadcastPublisher.publish(broadcast);
						} catch (IOException e) {
							StratusAPI.get().getLogger().severe("Failed to send global broadcast" + e);
							e.printStackTrace();
						}
					}
				}.runTaskAsynchronously(StratusAPI.get());
			} else {
				throw new CommandException("You do not have permission to send global broadcasts!");
			}
		}
	}

}
