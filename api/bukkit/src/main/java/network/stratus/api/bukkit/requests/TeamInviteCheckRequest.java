/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.UUID;

import network.stratus.api.bukkit.responses.TeamInviteCheckResponse;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * Represents a request to check if a player has team invites.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamInviteCheckRequest implements Request<TeamInviteCheckResponse> {

	private UUID uuid;

	public TeamInviteCheckRequest(UUID uuid) {
		this.uuid = uuid;
	}

	public UUID getUuid() {
		return this.uuid;
	}

	@Override
	public String getEndpoint() {
		return "/teams/invites/player/" + uuid.toString();
	}

	@Override
	public Class<TeamInviteCheckResponse> getResponseType() {
		return TeamInviteCheckResponse.class;
	}

	@Override
	public TeamInviteCheckResponse make(APIClient client) {
		return client.get(this);
	}

}
