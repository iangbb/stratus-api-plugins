/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models;

import java.util.Optional;

import javax.annotation.Nullable;

import org.bukkit.entity.Player;

/**
 * Represents a potentially non-existent {@link Player} for tab completion.
 * Allows for commands which send requests to the API to support both tab
 * completion of online players, and sending requests for offline players.
 * 
 * @author Ian Ballingall
 *
 */
public class TabCompletePlayer {

	private String username;
	private Optional<Player> player;

	public TabCompletePlayer(String username) {
		this.username = username;
		this.player = Optional.empty();
	}

	public TabCompletePlayer(String username, @Nullable Player player) {
		this.username = username;
		this.player = Optional.ofNullable(player);
	}

	public String getUsername() {
		return username;
	}

	public Optional<Player> getPlayer() {
		return player;
	}

	public boolean hasPlayer() {
		return player.isPresent();
	}

}
