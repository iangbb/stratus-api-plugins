/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.util;

/**
 * A simple util class to represent a pair of values
 *
 * @param <X> the type of the first value
 * @param <Y> the type of the s
 *
 * @author Matthew Arnold
 */
public class Pair<X, Y> {

    private final X first;
    private final Y second;

    private Pair(X first, Y second) {
        this.first = first;
        this.second = second;
    }

    public X first() {
        return first;
    }

    public Y second() {
        return second;
    }

    public static <X, Y> Pair<X, Y> of(X first, Y second) {
        return new Pair<>(first, second);
    }
}
