/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a ban, permanent or temporary.
 * 
 * @author Ian Ballingall
 *
 */
public class Ban extends BukkitPunishment {

	public Ban(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active, int number,
			String serverName, boolean silent) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
	}

	@Override
	public void enforce(boolean showServer) {
		initialisePlayers();

		final MultiAudience broadcast;
		final String permanent;
		final String permanentServer;
		final String temporary;
		final String temporaryServer;
		if (silent) {
			broadcast = new MultiAudience.Builder().includePermission("stratusapi.punishments.silent.see").build();
			permanent = "punishment.ban.permanent.silent";
			permanentServer = "punishment.ban.permanent.silent.server";
			temporary = "punishment.ban.temporary.silent";
			temporaryServer = "punishment.ban.temporary.silent.server";
		} else {
			broadcast = new MultiAudience.Builder().global().build();
			permanent = "punishment.ban.permanent.broadcast";
			permanentServer = "punishment.ban.permanent.broadcast.server";
			temporary = "punishment.ban.temporary.broadcast";
			temporaryServer = "punishment.ban.temporary.broadcast.server";
		}

		if (expiry == null) {
			if (showServer) {
				broadcast.sendMessage(permanentServer, serverName,
						Chat.getDisplayName(issuer),
						Chat.getDisplayName(target),
						reason);
			} else {
				broadcast.sendMessage(permanent,
						Chat.getDisplayName(issuer),
						Chat.getDisplayName(target),
						reason);
			}

			if (targetPlayer != null) {
				String kickMessage = StratusAPI.get().getTranslator().getStringOrDefaultLocale(targetPlayer.getLocale(),
						"punishment.ban.permanent.message");
				targetPlayer.kickPlayer(String.format(kickMessage, reason));
			}
		} else {
			if (showServer) {
				broadcast.sendMessage(temporaryServer, serverName,
						Chat.getDisplayName(issuer),
						Util.differenceInDays(time, expiry),
						Chat.getDisplayName(target),
						reason);
			} else {
				broadcast.sendMessage(temporary,
						Chat.getDisplayName(issuer),
						Util.differenceInDays(time, expiry),
						Chat.getDisplayName(target),
						reason);
			}

			if (targetPlayer != null) {
				String kickMessage = StratusAPI.get().getTranslator().getStringOrDefaultLocale(targetPlayer.getLocale(),
						"punishment.ban.temporary.message");
				targetPlayer.kickPlayer(String.format(kickMessage, reason, StratusAPI.get().getTranslator()
						.getTimeDifferenceString(targetPlayer.getLocale(), expiry, time)));
			}
		}
	}

	@Override
	public Type getType() {
		return Type.BAN;
	}

}
