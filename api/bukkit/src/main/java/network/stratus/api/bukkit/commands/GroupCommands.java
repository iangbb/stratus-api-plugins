/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.Set;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.models.UserData;
import network.stratus.api.bukkit.permissions.BukkitPermissionsManager;
import network.stratus.api.bukkit.requests.NameUserDataRequest;
import network.stratus.api.bukkit.requests.UserDataRequest;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.permissions.Group;
import network.stratus.api.requests.GroupMembershipModificationRequest;

/**
 * Commands pertaining to viewing and modifying players' group memberships.
 * 
 * @author Ian Ballingall
 *
 */
public class GroupCommands {

	@Command(aliases = "add",
			desc = "Add a user to a group",
			perms = "stratusapi.command.permissions.group",
			usage = "<username> <group>")
	public void addToGroup(CommandSender sender, TabCompletePlayer player, @Text String groupName) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		BukkitPermissionsManager bpm = StratusAPI.get().getPermissionsManager();
		StratusAPI.get().newSharedChain("groups").<Group>asyncFirst(() -> {
			new GroupMembershipModificationRequest(issuer, player.getUsername(), groupName, true)
					.make(StratusAPI.get().getApiClient());
			new SingleAudience(sender).sendMessage("permissions.group.update.success");
			return bpm.getGroupsManager().getStoredGroupByName(groupName);
		}).syncLast(group -> {
			if (player.getPlayer().isPresent()) {
				Player playerObject = player.getPlayer().get();
				bpm.setPermissions(playerObject, group);
				StratusAPI.get().getDisplayNameManager().setDisplayName(playerObject);
				new SingleAudience(playerObject).sendMessage("permissions.updated");
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "remove",
			desc = "Remove a user from a group",
			perms = "stratusapi.command.permissions.group",
			usage = "<username> <group>")
	public void removeFromGroup(CommandSender sender, TabCompletePlayer player, @Text String groupName) {
		UUID issuer = (sender instanceof Player) ? ((Player) sender).getUniqueId() : null;
		BukkitPermissionsManager bpm = StratusAPI.get().getPermissionsManager();
		StratusAPI.get().newSharedChain("groups").<Group>asyncFirst(() -> {
			new GroupMembershipModificationRequest(issuer, player.getUsername(), groupName, false)
					.make(StratusAPI.get().getApiClient());
			new SingleAudience(sender).sendMessage("permissions.group.update.success");
			return bpm.getGroupsManager().getStoredGroupByName(groupName);
		}).syncLast(group -> {
			// TODO: handle safely unsetting permissions
			// We don't want to unset the permissions if they are also part of another group
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "view",
			desc = "View a user's groups",
			perms = "stratusapi.command.permissions.group",
			usage = "<username>")
	public void viewGroups(CommandSender sender, TabCompletePlayer player) {
		UserDataRequest request = new NameUserDataRequest.Builder(player.getUsername()).groups().build();
		StratusAPI.get().newSharedChain("groups").<UserData>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(data -> {
			Set<Group> groups = data.getGroups();
			SingleAudience audience = new SingleAudience(sender);

			if (groups.isEmpty()) {
				audience.sendMessage("permissions.group.view.empty", Chat.getDisplayName(data));
			} else {
				audience.sendMessage("permissions.group.view.title", Chat.getDisplayName(data));
				for (Group group : groups) {
					audience.sendMessageRaw(ChatColor.AQUA + group.getName());
				}
			}
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
