/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models;

import java.util.Date;

import org.bukkit.Sound;
import com.fasterxml.jackson.annotation.JsonIgnore;

import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.models.Report;
import network.stratus.api.models.User;

/**
 * A {@link Report} with additional Bukkit-specific functionality.
 * 
 * @author Ian Ballingall
 *
 */
public class BukkitReport extends Report {

	public BukkitReport() {
		super();
	}

	public BukkitReport(User reporter, User target, Date time, String reason, String serverName) {
		super(reporter, target, time, reason, serverName);
	}

	public BukkitReport(String _id, User reporter, User target, Date time, String reason, String serverName) {
		super(_id, reporter, target, time, reason, serverName);
	}

	/**
	 * Broadcasts the report message to staff members.
	 * 
	 * @param showServer Whether to include the server in the message
	 */
	@JsonIgnore
	public void broadcast(boolean showServer) {
		MultiAudience staff = new MultiAudience.Builder().includePermission("stratusapi.reports.see").build();
		if (showServer) {
			staff.sendMessage("report.broadcast.server", getServerName(),
					Chat.getDisplayName(getReporter()),
					Chat.getDisplayName(getTarget()),
					getReason());
		} else {
			staff.sendMessage("report.broadcast",
					Chat.getDisplayName(getReporter()),
					Chat.getDisplayName(getTarget()),
					getReason());
		}
		staff.playSound(Sound.ENDERMAN_SCREAM, 1, 1);
	}

	/**
	 * Transform a deserialised {@link Report} into a {@code BukkitReport} to access
	 * Bukkit-specific features.
	 * 
	 * @param report The original {@link Report}
	 * @return A new {@code BukkitReport}
	 */
	public static BukkitReport fromReport(Report report) {
		return new BukkitReport(report.get_id(), report.getReporter(), report.getTarget(), report.getTime(),
				report.getReason(), report.getServerName());
	}

}
