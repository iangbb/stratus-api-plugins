/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.listeners;

import java.util.Date;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitRunnable;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.models.Session;
import network.stratus.api.server.SessionManager;

/**
 * Contains listeners pertaining to user disconnections.
 *
 * @author Ian Ballingall
 */
public class QuitListener implements Listener {

	/**
	 * Called when the user disconnects from the server. This detaches the user from
	 * the plugin with respect to permissions handling and notifies the API so that
	 * sessions can be appropriately terminated.
	 *
	 * @param event The quit event
	 */
	@EventHandler(priority = EventPriority.MONITOR)
	public void onQuit(PlayerQuitEvent event) {
		final Player player = event.getPlayer();
		StratusAPI.get().getPermissionsManager().detachPermissions(player);
		StratusAPI.get().getMuteManager().removeMute(player.getUniqueId());
		StratusAPI.get().getMuteManager().removeMuteTask(player.getUniqueId());

		final SessionManager sessionManager = StratusAPI.get().getSessionManager();
		new BukkitRunnable() {
			@Override
			public void run() {
				sessionManager.terminateSession(player.getUniqueId(),
						StratusAPI.get().getApiClient(),
						() -> new Session(null,
								player.getUniqueId(),
								player.getAddress().getHostString(),
								new Date(),
								new Date()));
			}
		}.runTaskAsynchronously(StratusAPI.get());
	}

}
