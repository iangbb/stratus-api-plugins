/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.models.punishment.PunishmentFactory.Type;
import network.stratus.api.util.Util;

/**
 * A request to view a punishment by the target's username and the number. The
 * number is as appears on /l.
 * 
 * @author Ian Ballingall
 *
 */
public class PunishmentViewRequest implements Request<BukkitPunishmentFactory> {

	private String username;
	private int number;
	private List<Type> types;

	public PunishmentViewRequest(String username, int number, List<Type> types) {
		this.username = username;
		this.number = number;
		this.types = types;
	}

	public PunishmentViewRequest(String username, int number, Type... types) {
		this.username = username;
		this.number = number;
		this.types = Arrays.asList(types);
	}

	public String getUsername() {
		return username;
	}

	public int getNumber() {
		return number;
	}

	public List<Type> getTypes() {
		return types;
	}

	@Override
	public String getEndpoint() {
		return "/punishments/name/" + username.replaceAll("/", "%2F") + "/" + number;
	}

	@Override
	public Map<String, Object> getProperties() {
		Map<String, Object> props = new HashMap<>();
		props.put("types", Util.buildPropertiesListString(types));
		return props;
	}

	@Override
	public Class<BukkitPunishmentFactory> getResponseType() {
		return BukkitPunishmentFactory.class;
	}

	@Override
	public BukkitPunishmentFactory make(APIClient client) {
		return client.get(this);
	}

}
