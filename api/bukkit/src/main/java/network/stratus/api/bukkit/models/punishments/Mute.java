/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models.punishments;

import java.util.Date;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.Chat;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.MuteManager;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.tasks.TimedPunishmentTask;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.models.User;
import network.stratus.api.models.punishment.PunishmentFactory.Type;

/**
 * Represents a mute, preventing a player from interacting in chat.
 * 
 * @author Ian Ballingall
 *
 */
public class Mute extends TimedBukkitPunishment {

	public Mute(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active, int number,
			String serverName, long timeRemaining) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, timeRemaining, false);
	}

	@Override
	public void enforce(boolean showServer) {
		initialisePlayers();
		MultiAudience staff = new MultiAudience.Builder().includePermission("stratusapi.punishments.mutes.see.others")
				.build();

		Date end = new Date(new Date().getTime() + getTimeRemaining());
		if (showServer) {
			staff.sendMessage("punishment.mute.broadcast.server", serverName,
					Chat.getDisplayName(issuer),
					Chat.getDisplayName(target),
					Util.differenceInHours(time, end),
					reason);
		} else {
			staff.sendMessage("punishment.mute.broadcast",
					Chat.getDisplayName(issuer),
					Chat.getDisplayName(target),
					Util.differenceInHours(time, end),
					reason);
		}

		if (targetPlayer != null) {
			MuteManager manager = StratusAPI.get().getMuteManager();
			manager.addMute(this);
			SingleAudience audience = new SingleAudience(targetPlayer);
			audience.sendMessage("punishment.mute.message.1");
			audience.sendMessage("punishment.mute.message.2", reason);
			audience.sendMessage("punishment.mute.message.3",
					StratusAPI.get().getTranslator().getTimeDifferenceString(targetPlayer.getLocale(), end, time));

			if (manager.isOnlineTimeGameplay()) {
				manager.addMuteTask(getTarget().get_id(),
						new TimedPunishmentTask.Builder(this).setPeriodToConfigurationValue().build().execute());
			}
		}
	}

	@Override
	public Type getType() {
		return Type.MUTE;
	}

}
