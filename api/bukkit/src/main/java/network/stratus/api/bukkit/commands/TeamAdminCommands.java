/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.commands;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import app.ashcon.intake.parametric.annotation.Text;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.models.TabCompletePlayer;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.requests.teams.TeamAdminDisbandRequest;
import network.stratus.api.requests.teams.TeamAdminRenameRequest;
import network.stratus.api.requests.teams.TeamAdminRequest;

/**
 * Commands for senior referees to manage teams.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamAdminCommands {

	private Map<UUID, String> teamRenameMap = new HashMap<>();

	@Command(aliases = "add",
			desc = "Add a player to a team",
			perms = "stratusapi.command.team.admin.add",
			usage = "<username> <team>")
	public void addToTeam(CommandSender sender, TabCompletePlayer target, @Text String team) {
		TeamAdminRequest request = TeamAdminRequest.addPlayer(team, target.getUsername());
		StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.admin.addplayer.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "remove",
			desc = "Remove a player from a team",
			perms = "stratusapi.command.team.admin.remove",
			usage = "<username> <team>")
	public void removeFromTeam(CommandSender sender, TabCompletePlayer target, @Text String team) {
		TeamAdminRequest request = TeamAdminRequest.removePlayer(team, target.getUsername());
		StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.admin.removeplayer.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "setleader",
			desc = "Set a team's leader",
			perms = "stratusapi.command.team.admin.setleader",
			usage = "<username> <team>")
	public void setLeader(CommandSender sender, TabCompletePlayer target, @Text String team) {
		TeamAdminRequest request = TeamAdminRequest.promotePlayer(team, target.getUsername());
		StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.admin.promote.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "disband",
			desc = "Forcefully disband a team",
			perms = "stratusapi.command.team.admin.disband",
			usage = "<username> <team>")
	public void disbandTeam(CommandSender sender, @Text String team) {
		TeamAdminDisbandRequest request = new TeamAdminDisbandRequest(team);
		StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			new SingleAudience(sender).sendMessage("team.admin.disband.success");
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

	@Command(aliases = "rename",
			desc = "Change a team's name",
			perms = "stratusapi.command.team.admin.rename",
			usage = "<team name>")
	public void rename(@Sender CommandSender sender, @Text String teamNameArgument) {
		SingleAudience audience = new SingleAudience(sender);
		UUID uuid = ((Player) sender).getUniqueId();
		String oldName = teamRenameMap.remove(uuid);
		if (oldName == null) {
			// No old name stored, so we're picking the name of the old team
			teamRenameMap.put(uuid, teamNameArgument);
			audience.sendMessage("team.admin.rename.queued.1", teamNameArgument);
			audience.sendMessage("team.admin.rename.queued.2");
			audience.sendMessage("team.admin.rename.queued.3");
		} else {
			// Old name stored, so perform the rename operation
			TeamAdminRenameRequest request = new TeamAdminRenameRequest(oldName, teamNameArgument);
			StratusAPI.get().newSharedChain("teams").<Void>asyncFirst(() -> {
				return request.make(StratusAPI.get().getApiClient());
			}).syncLast(response -> {
				audience.sendMessage("team.admin.rename.success");
			}).execute((e, task) -> Util.handleCommandException(e, task, sender));
		}
	}

	@Command(aliases = "cancelrename",
			desc = "Cancel a team rename",
			perms = "stratusapi.command.team.admin.rename")
	public void cancelRename(@Sender CommandSender sender) {
		SingleAudience audience = new SingleAudience(sender);
		String oldName = teamRenameMap.remove(((Player) sender).getUniqueId());
		if (oldName == null) {
			audience.sendMessage("team.admin.rename.cancel.norenamequeued");
		} else {
			audience.sendMessage("team.admin.rename.cancel.success", oldName);
		}
	}

}
