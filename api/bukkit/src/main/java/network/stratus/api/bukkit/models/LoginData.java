/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.models;

import java.util.SortedSet;

import network.stratus.api.models.punishment.TimedPunishment;
import network.stratus.api.permissions.Group;

/**
 * Stores data retrieved from the API as part of a
 * {@link org.bukkit.entity.Player}'s login. This data can then be used once the
 * Player object has been instantiated to apply properties to the player.
 * 
 * @author Ian Ballingall
 *
 */
public class LoginData {

	private SortedSet<Group> groups;
	private TimedPunishment mute;

	public LoginData(SortedSet<Group> groups, TimedPunishment mute) {
		this.groups = groups;
		this.mute = mute;
	}

	public SortedSet<Group> getGroups() {
		return groups;
	}

	public TimedPunishment getMute() {
		return mute;
	}

}
