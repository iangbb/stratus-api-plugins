/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.bukkit.requests;

import java.util.Map;
import java.util.UUID;

import network.stratus.api.bukkit.models.punishments.BukkitPunishmentFactory;
import network.stratus.api.requests.PunishmentModificationRequest;

/**
 * Bukkit implementation of the {@link PunishmentModificationRequest}. Allows
 * the updated punishment to be returned as a {@link BukkitPunishmentFactory}.
 * 
 * @author Ian Ballingall
 *
 */
public class BukkitPunishmentModificationRequest extends PunishmentModificationRequest<BukkitPunishmentFactory> {

	protected BukkitPunishmentModificationRequest(String username, int number, UUID sender, boolean canModifyOthers,
			Map<String, Object> changes) {
		super(username, number, sender, canModifyOthers, changes);
	}

	@Override
	public Class<BukkitPunishmentFactory> getResponseType() {
		return BukkitPunishmentFactory.class;
	}

	public static class Builder extends PunishmentModificationRequest.Builder<BukkitPunishmentFactory> {

		public Builder(String username, int number, UUID sender, boolean canModifyOthers) {
			super(username, number, sender, canModifyOthers);
		}

		@Override
		public PunishmentModificationRequest<BukkitPunishmentFactory> build() {
			return new BukkitPunishmentModificationRequest(username, number, sender, canModifyOthers, changes);
		}

	}

}
