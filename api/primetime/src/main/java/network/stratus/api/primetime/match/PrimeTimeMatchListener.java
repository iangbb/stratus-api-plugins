/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.primetime.match;

import java.util.Iterator;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.MatchListener;
import network.stratus.api.pgm.match.ParticipantManager;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.MatchManager;
import tc.oc.pgm.api.match.MatchPhase;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.event.MatchPlayerAddEvent;
import tc.oc.pgm.blitz.BlitzMatchModule;
import tc.oc.pgm.events.PlayerParticipationStartEvent;
import tc.oc.pgm.events.PlayerParticipationStopEvent;
import tc.oc.pgm.lib.net.kyori.text.TextComponent;
import tc.oc.pgm.teams.Team;

/**
 * Listens on events to manage Prime Time matches.
 * 
 * @author Ian Ballingall
 *
 */
public class PrimeTimeMatchListener implements MatchListener {

	@Override
	@EventHandler
	public void onPlayerJoinTeam(PlayerParticipationStartEvent event) {
		Optional<Party> party = StratusAPIPGM.get().getParticipantManager().getRegisteredParty(event.getMatch(),
				event.getPlayer().getId());
		Player player = Bukkit.getPlayer(event.getPlayer().getId());

		if (party.isPresent()) {
			if (!party.get().equals(event.getCompetitor())) {
				String message = String.format(StratusAPI.get().getTranslator().getStringOrDefaultLocale(
						player.getLocale(), "teamchange.rejected.wrongteam"), party.get().getName());
				event.cancel(TextComponent.of(message));
			}
		} else {
			String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(),
					"teamchange.rejected.notregistered");
			event.cancel(TextComponent.of(message));
		}
	}

	@Override
	@EventHandler
	public void onPlayerLeaveTeam(PlayerParticipationStopEvent event) {
		if (!StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch()))
			return;

		MatchPlayer matchPlayer = event.getPlayer();
		if (matchPlayer != null) {
			Player player = Bukkit.getPlayer(matchPlayer.getId());
			if (player != null && player.isOnline()
					&& StratusAPIPGM.get().getParticipantManager().isPlayerRegistered(event.getMatch(),
							player.getUniqueId())
					&& (!event.getMatch().hasModule(BlitzMatchModule.class)
							|| event.getMatch().getPhase() != MatchPhase.RUNNING)) {
				String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(player.getLocale(),
						"teamchange.rejected");
				event.cancel(TextComponent.of(message));
			}
		}
	}

	@Override
	@EventHandler
	public void onPlayerEnterMatch(MatchPlayerAddEvent event) {
		Optional<Party> registeredParty = StratusAPIPGM.get().getParticipantManager()
				.getRegisteredParty(event.getMatch(), event.getPlayer().getId());
		if (registeredParty.isPresent() && (!event.getMatch().hasModule(BlitzMatchModule.class)
				|| event.getMatch().getPhase() != MatchPhase.RUNNING)) {
			event.setInitialParty(registeredParty.get());
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void onLogin(PlayerLoginEvent event) {
		ParticipantManager pm = StratusAPIPGM.get().getParticipantManager();
		MatchManager mm = PGM.get().getMatchManager();

		Optional<Party> party = Optional.empty();
		Iterator<Match> matches = mm.getMatches();
		while (matches.hasNext() && !party.isPresent()) {
			party = pm.getRegisteredParty(matches.next(), event.getPlayer().getUniqueId());
		}

		if (party.isPresent()) {
			if (party.get() instanceof Team && ((Team) party.get()).getFullness(false) >= 1.0f) {
				String message = StratusAPI.get().getTranslator()
						.getStringOrDefaultLocale(event.getPlayer().getLocale(), "disconnect.teamfull");
				event.disallow(Result.KICK_FULL, message);
			}
		} else if (!event.getPlayer().hasPermission("primetime.spectate")) {
			String message = StratusAPI.get().getTranslator().getStringOrDefaultLocale(event.getPlayer().getLocale(),
					"disconnect.nospectate");
			event.disallow(Result.KICK_WHITELIST, message);
		}
	}

	@Override
	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIPGM.get().getReadyManager().newMatch(event.getMatch());
		ParticipantManager pm = StratusAPIPGM.get().getParticipantManager();
		pm.newMatch(event.getMatch());
		if (pm instanceof PrimeTimeParticipantManager) {
			PrimeTimeParticipantManager ptpm = (PrimeTimeParticipantManager) pm;
			ptpm.registerAllTeams(event.getMatch());
		} else {
			throw new IllegalStateException("Incompatible ParticipantManager type: " + pm.getClass().getSimpleName());
		}
	}

}
