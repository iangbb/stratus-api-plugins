/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.statistics;

import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.tasks.PlaytimeTask;
import tc.oc.pgm.api.match.Match;

/**
 * Manages statistics for matches.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsManager {

	private MatchStatistics matchStatistics;
	private Map<UUID, BukkitTask> playtimeTasks;

	public StatisticsManager() {
		this.playtimeTasks = new ConcurrentHashMap<>();
	}

	/**
	 * Initialise a new statistics tracker for the match.
	 */
	public void newMatch() {
		this.matchStatistics = new MatchStatistics();
	}

	/**
	 * Notify termination of the match, giving a requestable object for the match's
	 * statistics.
	 * 
	 * @return The statistics for the match
	 */
	public MatchStatistics endMatch() {
		matchStatistics.setMatchActive(false);
		clearPlaytimeTasks();
		return matchStatistics;
	}

	/**
	 * Begins tracking statistics for this player.
	 * 
	 * @param player The {@link UUID} of the player to track
	 */
	public void trackPlayer(UUID player) {
		matchStatistics.getTrackedPlayers().add(player);
	}

	/**
	 * Begins tracking statistics for the given collection of players.
	 * 
	 * @param players The {@link Collection} of {@link UUID}s of players to track
	 */
	public void trackPlayers(Collection<UUID> players) {
		matchStatistics.getTrackedPlayers().addAll(players);
	}

	/**
	 * Obtain the statistics records for the current match.
	 * 
	 * @return An object storing the statistcs for the current match
	 */
	public MatchStatistics getMatchStatistics() {
		return matchStatistics;
	}

	/**
	 * Increment the player's kill count by one.
	 * 
	 * @param player The UUID of the player who made the kill
	 * @return The player's new kill count
	 */
	public int registerKill(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger kills = matchStatistics.getKills().get(player);
		if (kills == null) {
			kills = new AtomicInteger();
			matchStatistics.getKills().put(player, kills);
		}

		return kills.incrementAndGet();
	}

	/**
	 * Increment the player's death count by one.
	 * 
	 * @param player The UUID of the player who died
	 * @return The player's new death count
	 */
	public int registerDeath(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger deaths = matchStatistics.getDeaths().get(player);
		if (deaths == null) {
			deaths = new AtomicInteger();
			matchStatistics.getDeaths().put(player, deaths);
		}

		return deaths.incrementAndGet();
	}

	/**
	 * Increment the number of wools captured by one.
	 * 
	 * @param player The UUID of the player who captured the wool
	 * @return The player's new wool capture count
	 */
	public int registerWool(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger wools = matchStatistics.getWools().get(player);
		if (wools == null) {
			wools = new AtomicInteger();
			matchStatistics.getWools().put(player, wools);
		}

		return wools.incrementAndGet();
	}

	/**
	 * Increment the number of cores leaked by one.
	 * 
	 * @param player The UUID of the player who leaked the core
	 * @return The player's new core leak count
	 */
	public int registerCore(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger cores = matchStatistics.getCores().get(player);
		if (cores == null) {
			cores = new AtomicInteger();
			matchStatistics.getCores().put(player, cores);
		}

		return cores.incrementAndGet();
	}

	/**
	 * Increment the number of monuments broken by one.
	 * 
	 * @param player The UUID of the player who broke the monument
	 * @return The player's new monument break count
	 */
	public int registerMonument(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger monuments = matchStatistics.getMonuments().get(player);
		if (monuments == null) {
			monuments = new AtomicInteger();
			matchStatistics.getMonuments().put(player, monuments);
		}

		return monuments.incrementAndGet();
	}

	/**
	 * Increment the number of flags captured by one.
	 * 
	 * @param player The UUID of the player who captured the flag
	 * @return The player's new flag capture count
	 */
	public int registerFlag(UUID player) {
		if (!matchStatistics.isMatchActive())
			throw new IllegalStateException("Cannot record statistics for inactive match");

		AtomicInteger flags = matchStatistics.getFlags().get(player);
		if (flags == null) {
			flags = new AtomicInteger();
			matchStatistics.getFlags().put(player, flags);
		}

		return flags.incrementAndGet();
	}

	/**
	 * Sends the given player a summary of their match statistics.
	 * 
	 * @param player The player to send a summary
	 */
	public void sendMatchStatisticsSummary(Player player) {
		UUID uuid = player.getUniqueId();
		int kills = matchStatistics.getKills(uuid);
		int deaths = matchStatistics.getDeaths(uuid);
		int wools = matchStatistics.getWools(uuid);
		int cores = matchStatistics.getCores(uuid);
		int monuments = matchStatistics.getMonuments(uuid);
		int flags = matchStatistics.getFlags(uuid);

		SingleAudience audience = new SingleAudience(player);
		audience.sendMessage("statistics.summary");
		audience.sendMessage("statistics.view.killsdeaths", kills, deaths,
				(deaths == 0) ? (float) kills : (float) kills / deaths);
		audience.sendMessage("statistics.view.objectives.title");
		audience.sendMessage("statistics.view.objectives", wools, cores, monuments, flags);
		if (matchStatistics.getRankedId() != null) {
			player.sendMessage(ChatColor.GOLD + "Match: https://stratus.network/m/" + matchStatistics.getRankedId());
		}
	}

	public void sendMatchStatisticsSummary(UUID uuid) {
		Player player = StratusAPIPGM.get().getServer().getPlayer(uuid);
		if (player != null)
			sendMatchStatisticsSummary(player);
	}

	/**
	 * Start a new task to track a given player's playtime. If one already exists,
	 * it will be cancelled and replaced with the new task.
	 * 
	 * @param uuid  The player's UUID
	 * @param match The match the player is in
	 */
	public void newPlaytimeTask(UUID uuid, Match match) {
		matchStatistics.getPlaytime().putIfAbsent(uuid, new AtomicInteger());
		BukkitTask task = new PlaytimeTask(uuid, matchStatistics.getPlaytime().get(uuid), match)
				.runTaskTimerAsynchronously(StratusAPIPGM.get(), 1200, 1200);
		BukkitTask oldTask = playtimeTasks.put(uuid, task);
		if (oldTask != null) {
			oldTask.cancel();
		}
	}

	/**
	 * Remove a player's playtime tracking task from the list and cancel it, if one
	 * exists.
	 * 
	 * @param uuid The player's UUID
	 */
	public void removePlaytimeTask(UUID uuid) {
		BukkitTask oldTask = playtimeTasks.remove(uuid);
		if (oldTask != null) {
			oldTask.cancel();
		}
	}

	/**
	 * Cancel all active playtime tasks and reset the map.
	 */
	private void clearPlaytimeTasks() {
		playtimeTasks.forEach((uuid, task) -> task.cancel());
		playtimeTasks.clear();
	}

}
