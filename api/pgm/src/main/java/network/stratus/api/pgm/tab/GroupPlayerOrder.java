/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.tab;

import java.util.SortedSet;

import org.bukkit.entity.Player;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.permissions.Group;
import network.stratus.api.pgm.StratusAPIPGM;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.tablist.PlayerOrder;
import tc.oc.pgm.tablist.PlayerOrderFactory;

/**
 * Orders the tab list based on the priority of each {@link MatchPlayer}'s top
 * {@link Group}. That is, their most important (lowest priority) group.
 * 
 * @author Ian Ballingall
 *
 */
public class GroupPlayerOrder extends PlayerOrder {

	public GroupPlayerOrder(MatchPlayer viewer) {
		super(viewer);
	}

	@Override
	public int compare(MatchPlayer ma, MatchPlayer mb) {
		if (getViewer() == ma) {
			return -1;
		} else if (getViewer() == mb) {
			return 1;
		}

		Group aTopGroup = getTopOrderedGroup(
				StratusAPI.get().getPermissionsManager().getGroupsManager().getPlayerGroups(ma.getId()));
		Group bTopGroup = getTopOrderedGroup(
				StratusAPI.get().getPermissionsManager().getGroupsManager().getPlayerGroups(mb.getId()));

		int groupComparison = aTopGroup.compareTo(bTopGroup);
		if (groupComparison == 0) {
			// Equal top group - apply alphabetical ordering
			Player a = StratusAPIPGM.get().getServer().getPlayer(ma.getId());
			Player b = StratusAPIPGM.get().getServer().getPlayer(mb.getId());
			Player viewer = StratusAPIPGM.get().getServer().getPlayer(getViewer().getId());
			return a.getName(viewer).compareToIgnoreCase(b.getName(viewer));
		} else {
			// Larger groups precede smaller groups
			return -groupComparison;
		}

	}

	private Group getTopOrderedGroup(SortedSet<Group> groups) {
		Group top = groups.first();
		for (Group group : groups) {
			if (group.isOrdered())
				top = group;
		}

		return top;
	}

	public static class Factory implements PlayerOrderFactory {

		@Override
		public PlayerOrder getOrder(MatchPlayer viewer) {
			return new GroupPlayerOrder(viewer);
		}

	}

}
