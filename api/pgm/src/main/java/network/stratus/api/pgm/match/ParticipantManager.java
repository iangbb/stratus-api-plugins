/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;

/**
 * Manages the tracking of participants in a PGM {@link Match}.
 * 
 * @author Ian Ballingall
 *
 */
public interface ParticipantManager {

	/**
	 * Registers the given participant as a member of the given team.
	 * 
	 * @param match The match the player is to be registered in
	 * @param uuid  The UUID of the player to register
	 * @param party The team the player is on
	 */
	public void registerParticipant(Match match, UUID uuid, Party party);

	/**
	 * Registers the given participant as a member of their current team.
	 * 
	 * @param player The player to register
	 */
	public void registerParticipant(MatchPlayer player);

	/**
	 * Convenience method for registering a list of players to their teams.
	 * 
	 * @param players The list of players to register
	 */
	public void registerParticipants(Collection<MatchPlayer> players);

	/**
	 * Removes a player's participation registration. They will not longer be
	 * associated with any team.
	 *
	 * @param match The match the player is to be unregistered from
	 * @param uuid  The UUID of the player to remove
	 * @return Whether the player was registered
	 */
	public boolean unregisterParticipant(Match match, UUID uuid);

	/**
	 * Obtain the team the player is registered against.
	 * 
	 * @param match The match which should be checked
	 * @param uuid  The UUID of the player
	 * @return The team they are registered against, contained within an Optional
	 */
	public Optional<Party> getRegisteredParty(Match match, UUID uuid);

	/**
	 * Get the register participants for a {@link Match}.
	 * 
	 * @param match The {@link Match} to check
	 * @return The {@link Map} of {@link Party}s to their members' {@link UUID}s
	 */
	public Map<UUID, Party> getParticipants(Match match);

	/**
	 * Check if a player is registered in this match.
	 *
	 * @param match The match which should be checked
	 * @param uuid  The UUID of the player
	 * @return Whether they are registered to a team
	 */
	public boolean isPlayerRegistered(Match match, UUID uuid);

	/**
	 * Inform the manager that a new match has loaded.
	 *
	 * @param match The match which has loaded
	 */
	public void newMatch(Match match);

	/**
	 * Inform the manager that the match has ended.
	 * 
	 * @param match The match which has ended
	 */
	public void endMatch(Match match);

	/**
	 * Check if the given match is registered with the manager.
	 * 
	 * @param match The match
	 * @return Whether the match is registered
	 */
	public boolean isMatchRegistered(Match match);

}
