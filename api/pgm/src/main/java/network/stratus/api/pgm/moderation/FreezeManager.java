/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.moderation;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;

import org.bukkit.Sound;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

/**
 * Controls player freezing and those in freeze mode.
 *
 * @author Meeples10
 * @author ThatOneTqnk
 */
public class FreezeManager {

	private Set<Player> frozenPlayers;

	public FreezeManager() {
		 frozenPlayers = new HashSet<>();
	}

	public void toggleFreeze(Player freezer, Player target) {
		if (isFrozen(target)) unfreezePlayer(freezer, target);
		else freezePlayer(freezer, target);
	}

	public void freezePlayer(Player freezer, Player target) {
		if (target.hasPermission("stratusapi.command.freeze")) {
			new SingleAudience(freezer).sendMessage("punishment.freeze.exempt", target.getName());
			return;
		}

		frozenPlayers.add(target);

		// Only broadcast freezes to staff members
		new MultiAudience.Builder().includePermission("stratusapi.punishments.freeze.see.others").build().sendMessage("punishment.freeze.broadcast",
				freezer.getDisplayName(), target.getDisplayName());

		SingleAudience audience = new SingleAudience(target);

		// Chat messages
		audience.sendMessage("punishment.freeze.title.chat");
		audience.sendMessage("punishment.freeze.message.1");
		audience.sendMessage("punishment.freeze.message.2");
		audience.sendMessage("punishment.freeze.message.3");
		audience.sendMessage("punishment.freeze.title.chat");

		// Title message
		audience.sendTitle(StratusAPI.get().getTranslator().getStringOrDefaultLocale(target.getLocale(),
				"punishment.freeze.title"), "", 5);

		// Sound
		audience.playSound(target.getLocation(), Sound.ENDERDRAGON_GROWL, 10, 1);
	}

	public void unfreezePlayer(Player freezer, Player target) {
		frozenPlayers.remove(target);
		new MultiAudience.Builder().includePermission("stratusapi.punishments.freeze.see.others").build()
				.sendMessage("punishment.freeze.broadcast.unfreeze", freezer.getDisplayName(), target.getDisplayName());
		new SingleAudience(target).sendMessage("punishment.freeze.unfrozen");
	}

	public boolean isFrozen(Player player) {
		return frozenPlayers.contains(player);
	}
}