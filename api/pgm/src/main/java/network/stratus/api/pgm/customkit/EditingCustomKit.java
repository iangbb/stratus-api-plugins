/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.customkit;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Manages the editing/creation of a custom kit by a player, tracking the order
 * of inventory slots.
 * 
 * @author ShinyDialga
 *
 */
public class EditingCustomKit {

	int lastSlot;
	int[] slots;

	public EditingCustomKit(int[] slots) {
		lastSlot = -1;
		this.slots = slots;
	}

	public int[] getSlots() {
		return slots;
	}

	public void updateSlot(Player player, ItemStack clickedItem, int slot, InventoryClickEvent event) {
		if (lastSlot >= 0) {
			int temp = slots[lastSlot];
			slots[lastSlot] = slots[slot];
			slots[slot] = temp;

			ItemStack lastItem = player.getInventory().getItem(lastSlot);
			player.getInventory().setItem(lastSlot, clickedItem);
			player.getInventory().setItem(slot, lastItem);

			event.setCancelled(true);

			lastSlot = -1;
		} else if (!clickedItem.getType().equals(Material.AIR)) {
			lastSlot = slot;
		}
		player.updateInventory();
	}

}
