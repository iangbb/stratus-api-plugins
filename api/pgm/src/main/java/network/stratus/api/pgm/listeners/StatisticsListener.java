/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import network.stratus.api.pgm.statistics.MatchStatisticTeam;
import network.stratus.api.pgm.statistics.MatchStatistics;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.statistics.StatisticsManager;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.match.event.MatchStartEvent;
import tc.oc.pgm.api.party.Competitor;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.ParticipantState;
import tc.oc.pgm.api.player.event.MatchPlayerDeathEvent;
import tc.oc.pgm.api.setting.SettingKey;
import tc.oc.pgm.api.setting.SettingValue;
import tc.oc.pgm.core.CoreLeakEvent;
import tc.oc.pgm.destroyable.DestroyableContribution;
import tc.oc.pgm.destroyable.DestroyableDestroyedEvent;
import tc.oc.pgm.events.PlayerParticipationStartEvent;
import tc.oc.pgm.events.PlayerParticipationStopEvent;
import tc.oc.pgm.flag.event.FlagCaptureEvent;
import tc.oc.pgm.stats.PlayerStats;
import tc.oc.pgm.stats.StatsMatchModule;
import tc.oc.pgm.wool.PlayerWoolPlaceEvent;

/**
 * Listener methods pertaining to statistics. These listen to PGM events in
 * order to track player kills, deaths and objectives, and to know when matches
 * are starting and ending.
 * 
 * @author Ian Ballingall
 *
 */
public class StatisticsListener implements Listener {

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerDeath(MatchPlayerDeathEvent event) {
		if (!event.isTeamKill()) {
			StratusAPIPGM.get().getStatisticsManager().registerDeath(event.getVictim().getId());

			if (event.getKiller() != null && !event.isSelfKill() && !event.isSuicide()) {
				StratusAPIPGM.get().getStatisticsManager().registerKill(event.getKiller().getId());
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onCoreLeak(CoreLeakEvent event) {
		for (ParticipantState ps : event.getCore().getTouchingPlayers()) {
			StratusAPIPGM.get().getStatisticsManager().registerCore(ps.getId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMonumentBreak(DestroyableDestroyedEvent event) {
		for (DestroyableContribution dc : event.getDestroyable().getContributions()) {
			StratusAPIPGM.get().getStatisticsManager().registerMonument(dc.getPlayerState().getId());
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onWoolCapture(PlayerWoolPlaceEvent event) {
		StratusAPIPGM.get().getStatisticsManager().registerWool(event.getPlayer().getId());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onFlagCapture(FlagCaptureEvent event) {
		StratusAPIPGM.get().getStatisticsManager().registerFlag(event.getCarrier().getId());
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIPGM.get().getStatisticsManager().newMatch();
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchStart(MatchStartEvent event) {
		Match match = event.getMatch();
		for (MatchPlayer player : match.getParticipants()) {
			StratusAPIPGM.get().getStatisticsManager().newPlaytimeTask(player.getId(), match);
			StratusAPIPGM.get().getStatisticsManager().trackPlayers(
					event.getMatch().getParticipants().stream().map(MatchPlayer::getId).collect(Collectors.toList()));
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchFinish(MatchFinishEvent event) {
		StatisticsManager statsManager = StratusAPIPGM.get().getStatisticsManager();
		MatchStatistics matchStatistics = statsManager.getMatchStatistics();

		boolean registered = StratusAPIPGM.get().getParticipantManager().isMatchRegistered(event.getMatch());

		event.getMatch().getCompetitors().forEach(competitor -> {
			matchStatistics.getTeams().add(new MatchStatisticTeam(competitor.getNameLegacy(),
					competitor.getColor().name().toLowerCase(), event.getWinners().contains(competitor)));
		});

		matchStatistics.setMap(event.getMatch().getMap().getName());
		matchStatistics.setDuration((int)event.getMatch().getDuration().getSeconds());

		Map<UUID, Map<String, Object>> matchStats = getStats(event.getMatch());
		matchStatistics.setMatchStats(matchStats);

		event.getMatch().getCompetitors().forEach(competitor -> {
			competitor.getPlayers().forEach(matchPlayer -> {
				matchStatistics.getPlayerTeams().put(matchPlayer.getId(), matchPlayer.getParty().getNameLegacy());
			});
		});

		for (Competitor competitor : event.getWinners()) {
			if (registered) {
				Map<UUID, Party> participants = StratusAPIPGM.get().getParticipantManager().getParticipants(event.getMatch());
				for (UUID uuid : participants.keySet()) {
					Party party = participants.get(uuid);
					if (party != null && party.equals(competitor)) {
						matchStatistics.getWinners().add(uuid);
					}
				}
			}
			for (MatchPlayer player : competitor.getPlayers()) {
				matchStatistics.getWinners().add(player.getId());
			}
		}

		if (event.getWinners().size() > 0) {
			if (registered) {
				Map<UUID, Party> participants = StratusAPIPGM.get().getParticipantManager().getParticipants(event.getMatch());
				for (UUID uuid : participants.keySet()) {
					matchStatistics.getPlayerTeams().put(uuid, participants.get(uuid).getNameLegacy());

					if (!matchStatistics.getWinners().contains(uuid)) {
						matchStatistics.getLosers().add(uuid);
					}
				}
			}
            for (UUID uuid : matchStatistics.getTrackedPlayers()) {
                if (!matchStatistics.getWinners().contains(uuid)) {
					matchStatistics.getLosers().add(uuid);
                }
            }
        }


		// Delay to ensure the above events and others are handled before we display
		// statistics to players and submit them to the API, as well as make the message later
		StratusAPI.get().newSharedChain("statistics").delay(20 * 7).sync(() -> {
			event.getMatch().getParticipants().stream()
					.filter(player -> player.getSettings().getValue(SettingKey.STATS).equals(SettingValue.STATS_ON))
					.map(MatchPlayer::getId).forEach(statsManager::sendMatchStatisticsSummary);
		}).async(() -> {
			statsManager.endMatch().make(StratusAPI.get().getApiClient());
		}).execute();
	}

	private Map<UUID, Map<String, Object>> getStats(Match match) {
		Map<UUID, Map<String, Object>> matchStats = new HashMap<>();

		StatsMatchModule smm = match.needModule(StatsMatchModule.class);
		if (smm != null) {
			smm.getAllPlayerStats().forEach((uuid, stats) -> {
				Map<String, Object> playerStats = new HashMap<>();
//				playerStats.put("arrowAccuracy", stats.getArrowAccuracy()); //redundant
				playerStats.put("bowDamage", stats.getBowDamage());
				playerStats.put("damageDone", stats.getDamageDone());
				playerStats.put("damageTaken", stats.getDamageTaken());
//				playerStats.put("kd", stats.getKD()); //redundant
				playerStats.put("deaths", stats.getDeaths());
				playerStats.put("destroyablePiecesBroken", stats.getDestroyablePiecesBroken());
				playerStats.put("flagsCaptured", stats.getFlagsCaptured());
				playerStats.put("kills", stats.getKills());
				playerStats.put("killstreak", stats.getKillstreak());
				playerStats.put("maxKillstreak", stats.getMaxKillstreak());
				playerStats.put("shotsHit", stats.getShotsHit());
				playerStats.put("shotsTaken", stats.getShotsTaken());
				playerStats.put("longestFlagHold", stats.getLongestFlagHold());
				playerStats.put("longestBowKill", stats.getLongestBowKill());
				matchStats.put(uuid, playerStats);
			});
		}

		return matchStats;
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onLeaveMatch(PlayerParticipationStopEvent event) {
		// Only display statistics if the match is active and leave was not cancelled
		if (!event.isCancelled() && event.getMatch().isRunning()) {
			MatchPlayer player = event.getPlayer();
			StatisticsManager statsManager = StratusAPIPGM.get().getStatisticsManager();
			statsManager.removePlaytimeTask(player.getId());

			if (player.getSettings().getValue(SettingKey.STATS).equals(SettingValue.STATS_ON)) {
				statsManager.sendMatchStatisticsSummary(player.getId());
			}
		}

	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onJoinMatch(PlayerParticipationStartEvent event) {
		if (!event.isCancelled() && event.getMatch().isRunning()) {
			StratusAPIPGM.get().getStatisticsManager().newPlaytimeTask(event.getPlayer().getId(), event.getMatch());
			StratusAPIPGM.get().getStatisticsManager().trackPlayer(event.getPlayer().getId());
		}
	}

}
