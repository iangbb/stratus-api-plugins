/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;

/**
 * A participation manager based on mapping participants' UUIDs to the party of
 * which they are a member.
 * 
 * @author Ian Ballingall
 *
 */
public class UuidMappedParticipantManager implements ParticipantManager {

	private Map<Match, Map<UUID, Party>> matches;

	public UuidMappedParticipantManager() {
		matches = new HashMap<>();
	}

	@Override
	public void registerParticipant(Match match, UUID uuid, Party party) {
		Map<UUID, Party> participants = getParticipants(match);
		participants.put(uuid, party);
	}

	@Override
	public void registerParticipant(MatchPlayer player) {
		registerParticipant(player.getMatch(), player.getId(), player.getParty());
	}

	@Override
	public void registerParticipants(Collection<MatchPlayer> players) {
		players.forEach(this::registerParticipant);
	}

	@Override
	public boolean unregisterParticipant(Match match, UUID uuid) {
		return getParticipants(match).remove(uuid) != null;
	}

	@Override
	public Optional<Party> getRegisteredParty(Match match, UUID uuid) {
		return Optional.ofNullable(getParticipants(match).get(uuid));
	}

	@Override
	public boolean isPlayerRegistered(Match match, UUID uuid) {
		return getParticipants(match).containsKey(uuid);
	}

	@Override
	public void newMatch(Match match) {
		if (matches.containsKey(match)) {
			return;
		}
		matches.put(match, new HashMap<>());
	}

	@Override
	public void endMatch(Match match) {
		matches.remove(match);
	}

	@Override
	public boolean isMatchRegistered(Match match) {
		return matches.containsKey(match);
	}

	@Override
	public Map<UUID, Party> getParticipants(Match match) {
		Map<UUID, Party> participants = matches.get(match);
		if (participants == null) {
			throw new IllegalArgumentException("Match is not registered");
		}

		return participants;
	}

}
