/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.tasks;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

import org.bukkit.scheduler.BukkitRunnable;

import tc.oc.pgm.api.match.Match;

/**
 * A task which tracks a player's playtime in the current match.
 * 
 * @author Ian Ballingall
 *
 */
public class PlaytimeTask extends BukkitRunnable {

	private final UUID uuid;
	private final AtomicInteger time;
	private final Match match;

	public PlaytimeTask(UUID uuid, AtomicInteger time, Match match) {
		this.uuid = uuid;
		this.time = time;
		this.match = match;
	}

	public UUID getUuid() {
		return uuid;
	}

	public int getTime() {
		return time.intValue();
	}

	public Match getMatch() {
		return match;
	}

	@Override
	public void run() {
		if (!match.isRunning()) {
			cancel();
			return;
		}

		time.getAndIncrement();
	}

}
