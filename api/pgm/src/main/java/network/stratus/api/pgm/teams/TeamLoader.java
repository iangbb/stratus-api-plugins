/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.teams;

import java.util.List;

/**
 * System for loading teams in from an external source, such as a configuration
 * file, database or the API.
 * 
 * @author Ian Ballingall
 *
 */
public interface TeamLoader {

	/**
	 * Retrieve all available teams.
	 * 
	 * @return A {@link List} of {@link Team}s
	 */
	List<Team> loadTeams();

	/**
	 * Retrieve a team by the given name.
	 * 
	 * @param name The name of the team
	 * @return The {@link Team} object
	 */
	Team loadTeam(String name);

}
