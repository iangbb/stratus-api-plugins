/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.commands;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.bukkit.util.Util;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.requests.CustomKitDeleteRequest;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;

/**
 * Commands for custom kits
 * 
 * @author ShinyDialga
 *
 */
public class CustomKitCommands {

	@Command(aliases = { "resetcustomkit" },
			desc = "Reset your custom kit for the current map")
	public void resetCustomKit(@Sender CommandSender sender) {
		Player player = (Player) sender;
		Match match = PGM.get().getMatchManager().getMatch(player);
		CustomKitDeleteRequest request = new CustomKitDeleteRequest(player.getUniqueId(), match.getMap().getId());

		StratusAPI.get().newSharedChain("customkit").asyncFirst(() -> request.make(StratusAPI.get().getApiClient())
		).syncLast(response -> {
			new SingleAudience(sender).sendMessage("customkit.reset");
			StratusAPIPGM.get().getKitTracker().removeKit(player.getUniqueId(), match.getMap().getId());
		}).execute((e, task) -> Util.handleCommandException(e, task, sender));
	}

}
