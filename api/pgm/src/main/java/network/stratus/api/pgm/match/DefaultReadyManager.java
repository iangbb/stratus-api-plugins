/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.match.QueuedParty;

/**
 * Manages the ready status for teams in a {@link Match}, mapping {@link Party}s
 * to their ready status.
 * 
 * @author Ian Ballingall
 *
 */
public class DefaultReadyManager implements ReadyManager {

	private Map<Party, Boolean> readyStatus;

	public DefaultReadyManager() {
		this.readyStatus = new HashMap<>();
	}

	@Override
	public void newMatch(Match match) {
		match.getParties().stream().filter(party -> !(party instanceof QueuedParty) && !party.equals(match.getDefaultParty()))
				.forEach(party -> readyStatus.put(party, false));
	}

	@Override
	public void endMatch(Match match) {
		match.getParties().forEach(readyStatus::remove);
	}

	@Override
	public boolean getReadyStatus(Party party) {
		if (!readyStatus.containsKey(party))
			throw new IllegalArgumentException("Party is not registered to match");

		return readyStatus.get(party);
	}

	@Override
	public boolean setReadyStatus(Party party, boolean status) {
		if (!readyStatus.containsKey(party))
			throw new IllegalArgumentException("Party is not registered to match");

		return readyStatus.put(party, status) != status;
	}

	@Override
	public boolean toggleReadyStatus(Party party) {
		if (!readyStatus.containsKey(party))
			throw new IllegalArgumentException("Party is not registered to match");

		return !readyStatus.put(party, !readyStatus.get(party));
	}

	@Override
	public boolean areAllTeamsReady(Match match) {
		for (Entry<Party, Boolean> e : readyStatus.entrySet()) {
			if (match.getParties().contains(e.getKey()) && !e.getValue()) {
				return false;
			}
		}

		return true;
	}

}
