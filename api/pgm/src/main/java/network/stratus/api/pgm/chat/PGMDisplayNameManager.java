/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.chat;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.DisplayNameManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import tc.oc.pgm.api.event.NameDecorationChangeEvent;
import tc.oc.pgm.util.named.NameDecorationProvider;

import java.util.UUID;

/**
 * Manages display names when using PGM. This acts as both the Stratus-API display name manager and as the prefix
 * provider for PGM. When the player's display name is updated by the API plugin, PGM is notified via a
 * {@link NameDecorationChangeEvent} and will retrieve the updated prefix accordingly.<br>
 * Suffixes are not presently supported.
 *
 * @author Ian Ballingall
 * @author Meeples10
 */
public class PGMDisplayNameManager implements DisplayNameManager, NameDecorationProvider {

	@Override
	public void setDisplayName(Player player) {
		Bukkit.getPluginManager().callEvent(new NameDecorationChangeEvent(player.getUniqueId()));
	}

	@Override
	public String getPrefix(UUID uuid) {
		return StratusAPI.get()
				.getPermissionsManager()
				.getGroupsManager()
				.getPrefixes(uuid, StratusAPI.get().getPermissionsManager().getRealms());
	}

	@Override
	public String getSuffix(UUID uuid) {
		return "";
	}

}
