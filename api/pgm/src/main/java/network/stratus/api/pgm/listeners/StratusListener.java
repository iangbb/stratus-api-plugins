/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.customkit.CustomKitTracker;
import network.stratus.api.pgm.customkit.EditingCustomKit;
import network.stratus.api.pgm.models.CustomKit;
import network.stratus.api.pgm.requests.CustomKitCreateRequest;
import network.stratus.api.pgm.requests.CustomKitViewRequest;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.map.MapInfo;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.api.player.event.MatchPlayerAddEvent;
import tc.oc.pgm.classes.ClassMatchModule;
import tc.oc.pgm.events.PlayerPartyChangeEvent;
import tc.oc.pgm.kits.ItemKitAddItemEvent;
import tc.oc.pgm.kits.Kit;
import tc.oc.pgm.kits.KitMatchModule;
import tc.oc.pgm.spawns.events.ObserverKitApplyEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.NotFoundException;

/**
 * Listens on events which occur during a match that are pertinent to managing
 * custom kits.
 *
 * @author ShinyDialga
 */
public class StratusListener implements Listener {

	private CustomKitTracker tracker;

	public StratusListener() {
		this.tracker = StratusAPIPGM.get().getKitTracker();
	}

	public static Map<Player, EditingCustomKit> currentlyEditing = new HashMap<>();

	private boolean isObserver(Player player) {
		Match match = PGM.get().getMatchManager().getMatch(player);
		if (match == null) {
			return false;
		}
		MatchPlayer player1 = match.getPlayer(player);
		return player1 != null && player1.isObserving();
	}

	private void handleGUI(Player player) {
		Match match = PGM.get().getMatchManager().getMatch(player);
		if (match.hasModule(KitMatchModule.class) && !match.hasModule(ClassMatchModule.class)) {
			MatchPlayer player1 = match.getPlayer(player);
			player.getInventory().clear();
			player.setAllowFlight(true);
			player.setFlying(true);
			player.setGameMode(GameMode.ADVENTURE);
			player.setAllowFlight(true);
			player.setFlying(true);
			StratusAPIPGM.get().getServer().getScheduler().scheduleSyncDelayedTask(StratusAPIPGM.get(), new Runnable() {
				@Override
				public void run() {
					player.setAllowFlight(true);
					player.setFlying(true);
				}
			}, 1L);

			int[] slots = new int[36];

			for (int i = 0; i < 36; i++) {
				slots[i] = i;
			}

			currentlyEditing.put(player, new EditingCustomKit(slots));

			for (Kit kit : match.needModule(KitMatchModule.class).getModule().getKits()) {
				kit.apply(player1, true, new ArrayList<>());
			}
			;

			player.getInventory().setArmorContents(new ItemStack[] { null, null, null, null });

			SingleAudience audience = new SingleAudience(player);
			audience.sendMessage("customkit.edit.header");
			audience.sendMessage("customkit.edit.1");
			audience.sendMessage("customkit.edit.2");
			audience.sendMessage("customkit.edit.3");
			audience.sendMessage("customkit.edit.header");
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onCustomKitEdit(final InventoryClickEvent event) {
		if (isObserver(event.getActor()) && currentlyEditing.containsKey(event.getActor())) {
			int slot = event.getSlot();
			ClickType action = event.getClick();
			if (slot < 0 || slot > 35 ||
					!action.equals(ClickType.LEFT) || event.getSlotType().equals(InventoryType.SlotType.OUTSIDE)) {
				event.setCancelled(true);
			} else {
				event.setCancelled(false);

				currentlyEditing.get(event.getActor())
						.updateSlot(event.getActor(), event.getCurrentItem(), slot, event);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onItemKitAddItem(final ItemKitAddItemEvent event) {
		MatchPlayer player = event.getPlayer();

		MapInfo info = player.getMatch().getMap();
		CustomKit kit = tracker.getCustomKit(player.getId(), info.getId(), info.getVersion().toString());

		if (kit != null && !currentlyEditing.containsKey(player.getBukkit())) {
			if (event.getIndex() >= 0 && event.getIndex() <= 35) {
				for (int i = 0; i < 36; i++) {
					if (kit.getSlots()[i] == event.getIndex()) {
						event.setIndex(i);
						break;
					}
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onInventoryClose(final InventoryCloseEvent event) {
		if (currentlyEditing.containsKey(event.getActor())) {
			Player player = event.getActor();
			Match match = PGM.get().getMatchManager().getMatch(event.getActor());
			MatchPlayer player1 = match.getPlayer(event.getActor());
			if (player1.isObserving()) {
				event.getPlayer().setGameMode(GameMode.CREATIVE);
				event.getPlayer().getInventory().clear();
				match.callEvent(new ObserverKitApplyEvent(player1));

				int[] slots = currentlyEditing.get(player).getSlots();
				int[] defaultSlots = new int[36];
				for (int i = 0; i < 36; i++) {
					defaultSlots[i] = i;
				}

				boolean defaultKit = Arrays.equals(defaultSlots, slots);

				MapInfo info = player1.getMatch().getMap();
				CustomKit kit = tracker.getCustomKit(player.getUniqueId(), info.getId(), info.getVersion().toString());

				if (kit != null || !defaultKit) {
					CustomKitCreateRequest request = new CustomKitCreateRequest(player.getUniqueId(),
							match.getMap().getId(), match.getMap().getVersion().toString(),
							slots);
					StratusAPI.get().newSharedChain("customkit").<Void>asyncFirst(() -> {
						return request.make(StratusAPI.get().getApiClient());
					}).syncLast(response -> {
						tracker.addKit(player.getUniqueId(),
								match.getMap().getId(),
								new CustomKit(match.getMap().getId(), match.getMap().getVersion().toString(),
										currentlyEditing.get(player).getSlots()));
						new SingleAudience(player).sendMessage("customkit.saved");
						currentlyEditing.remove(event.getActor());

					}).execute();
				} else {
					currentlyEditing.remove(event.getActor());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerPartyChange(final PlayerPartyChangeEvent event) {
		event.getPlayer().getBukkit().getInventory().clear();
		if (event.getPlayer().isObserving()) {
			event.getPlayer().setGameMode(GameMode.CREATIVE);
			event.getPlayer().getInventory().clear();
			event.getPlayer().getMatch().callEvent(new ObserverKitApplyEvent(event.getPlayer()));
		}
		currentlyEditing.remove(event.getPlayer().getBukkit());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerQuit(final PlayerQuitEvent event) {
		tracker.removePlayer(event.getPlayer().getUniqueId());
		currentlyEditing.remove(event.getPlayer());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onMatchLoad(final MatchLoadEvent event) {
		currentlyEditing.clear();
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerClick(final PlayerInteractEvent event) {
		Material heldItemType = event.getPlayer().getItemInHand().getType();
		if (isObserver(event.getActor()) && (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) ||
				event.getAction().equals(Action.RIGHT_CLICK_AIR)) && heldItemType.equals(Material.CHEST) &&
				!currentlyEditing.containsKey(event.getPlayer())) {
			handleGUI(event.getActor());
			event.setCancelled(true);
		} else if (isObserver(event.getActor()) && (event.getAction().equals(Action.RIGHT_CLICK_BLOCK) ||
				event.getAction().equals(Action.RIGHT_CLICK_AIR)) && heldItemType.equals(Material.ENCHANTED_BOOK)) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onObserverKitApply(ObserverKitApplyEvent event) {
		ItemStack kit = new ItemStack(Material.CHEST);

		ItemMeta kitMeta = kit.getItemMeta();
		kitMeta.setDisplayName(new SingleAudience(event.getPlayer().getBukkit()).translate("customkit.item"));

		kit.setItemMeta(kitMeta);

		ItemStack server = new ItemStack(Material.ENCHANTED_BOOK);

		ItemMeta serverMeta = kit.getItemMeta();
		serverMeta.setDisplayName(new SingleAudience(event.getPlayer().getBukkit()).translate("serverpicker.name"));

		server.setItemMeta(serverMeta);

		event.getPlayer().getBukkit().getInventory().setItem(3, server);
		event.getPlayer().getBukkit().getInventory().setItem(6, kit);
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onPlayerEnterMatch(MatchPlayerAddEvent event) {
		MatchPlayer player = event.getPlayer();

		MapInfo map = player.getMatch().getMap();
		if (tracker.getCustomKit(player.getId(), map.getId(), map.getVersion().toString()) != null)
			return;

		CustomKitViewRequest request = new CustomKitViewRequest(player.getId(), map.getId(),
				map.getVersion().toString());
		StratusAPI.get().newSharedChain("customkit").<CustomKit>asyncFirst(() -> {
			return request.make(StratusAPI.get().getApiClient());
		}).syncLast(response -> {
			// Make sure player is still here before adding - don't store if they leave
			if (player.getBukkit() != null && player.getBukkit().isOnline()) {
				tracker.addKit(player.getId(), map.getId(), response);
			}
		}).execute((exception, task) -> {
			RequestFailureException rfe = null;
			if (exception instanceof RequestFailureException) {
				rfe = ((RequestFailureException) exception);
			}

			if (exception.getCause() != null && exception.getCause() instanceof NotFoundException) {
				StratusAPIPGM.get().getLogger().fine(rfe.getResponse().get().getDescription());
			} else {
				StratusAPIPGM.get().getLogger().severe("Exception occurred loading custom kits");
				if (rfe != null && rfe.getResponse().isPresent()) {
					StratusAPIPGM.get().getLogger().severe(rfe.getResponse().get().getDescription());
				}

				exception.printStackTrace();
			}
		});
	}

}
