/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * A request to delete a player's custom kit for a given map.
 * 
 * @author Ian Ballingall
 *
 */
public class CustomKitDeleteRequest implements Request<Void> {

	private UUID uuid;
	private String mapId;

	public CustomKitDeleteRequest(UUID uuid, String mapId) {
		this.uuid = uuid;
		this.mapId = mapId;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getMapId() {
		return mapId;
	}

	@Override
	public String getEndpoint() {
		return "/pgm/customkit/" + uuid.toString() + "/" + mapId;
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.delete(this);
	}

}
