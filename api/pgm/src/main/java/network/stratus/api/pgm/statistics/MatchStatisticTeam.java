package network.stratus.api.pgm.statistics;

public class MatchStatisticTeam {

    private String name;
    private String color;
    private boolean winner;

    public MatchStatisticTeam(String name, String color, boolean winner) {
        this.name = name;
        this.color = color;
        this.winner = winner;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public boolean isWinner() {
        return winner;
    }
}
