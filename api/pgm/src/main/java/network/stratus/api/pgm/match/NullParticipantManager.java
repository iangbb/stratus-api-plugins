/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.match;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;

/**
 * The default participant manager which does nothing.
 * 
 * @author Ian Ballingall
 *
 */
public class NullParticipantManager implements ParticipantManager {

	@Override
	public void registerParticipant(Match match, UUID uuid, Party party) {

	}

	@Override
	public void registerParticipant(MatchPlayer player) {

	}

	@Override
	public void registerParticipants(Collection<MatchPlayer> players) {

	}

	@Override
	public boolean unregisterParticipant(Match match, UUID uuid) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public Optional<Party> getRegisteredParty(Match match, UUID uuid) {
		return Optional.empty();
	}

	@Override
	public boolean isPlayerRegistered(Match match, UUID uuid) {
		return true;
	}

	@Override
	public void newMatch(Match match) {

	}

	@Override
	public void endMatch(Match match) {

	}

	@Override
	public boolean isMatchRegistered(Match match) {
		return false;
	}

	@Override
	public Map<UUID, Party> getParticipants(Match match) {
		return new HashMap<>();
	}

}
