/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.pgm.listeners;

import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.moderation.FreezeManager;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.bukkit.event.vehicle.VehicleEnterEvent;
import org.bukkit.event.vehicle.VehicleExitEvent;
import org.bukkit.event.vehicle.VehicleMoveEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.Vector;
import tc.oc.pgm.api.player.event.ObserverInteractEvent;
import tc.oc.pgm.spawns.events.ObserverKitApplyEvent;

/**
 * Prevents players from moving or interacting with the world while frozen.
 *
 * @author Meeples10
 * @author ThatOneTqnk
 */
public class FreezeListener implements Listener {

	private FreezeManager manager;

	public FreezeListener(FreezeManager manager) {
		this.manager = manager;
	}

	private boolean isPlayerFrozen(Entity potentialPlayer) {
		if (!(potentialPlayer instanceof Player))
			return false;
		return manager.isFrozen((Player) potentialPlayer);
	}

	private boolean isPlayerFrozen(Player player) {
		return manager.isFrozen(player);
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onPlayerInteractEntity(final ObserverInteractEvent event) {
		if (event.getPlayer().isDead())
			return;

		Player issuer = StratusAPIPGM.get().getServer().getPlayer(event.getPlayer().getId());
		if (this.isPlayerFrozen(issuer)) {
			event.setCancelled(true);

		} else if (event.getClickedItem() != null && event.getClickedItem().getType() == Material.ICE
				&& issuer.hasPermission("stratusapi.command.freeze") && event.getClickedPlayer() != null) {
			event.setCancelled(true);
			manager.toggleFreeze(issuer, StratusAPIPGM.get().getServer().getPlayer(event.getClickedPlayer().getId()));
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onPlayerMove(final PlayerMoveEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			Location old = event.getFrom();
			old.setPitch(event.getTo().getPitch());
			old.setYaw(event.getTo().getYaw());
			event.setTo(old);
		}
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void onVehicleMove(final VehicleMoveEvent event) {
		if (!event.getVehicle().isEmpty() && this.isPlayerFrozen(event.getVehicle().getPassenger())) {
			event.getVehicle().setVelocity(new Vector(0, 0, 0));
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onVehicleEnter(final VehicleEnterEvent event) {
		if (this.isPlayerFrozen(event.getEntered())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onVehicleExit(final VehicleExitEvent event) {
		if (this.isPlayerFrozen(event.getExited())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onBlockBreak(final BlockBreakEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onBlockPlace(final BlockPlaceEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onBucketFill(final PlayerBucketFillEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onBucketEmpty(final PlayerBucketEmptyEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW) // ignoreCancelled doesn't seem to work well here
	public void onPlayerInteract(final PlayerInteractEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onInventoryClick(final InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player) {
			if (this.isPlayerFrozen(event.getWhoClicked())) {
				event.setCancelled(true);
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onPlayerDropItem(final PlayerDropItemEvent event) {
		if (this.isPlayerFrozen(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onEntityDamge(final EntityDamageByEntityEvent event) {
		if (this.isPlayerFrozen(event.getDamager())) {
			event.setCancelled(true);
		}
	}

	@EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
	public void onVehicleDamage(final VehicleDamageEvent event) {
		if (this.isPlayerFrozen(event.getAttacker())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onObserverKitApply(ObserverKitApplyEvent event) {
		if (event.getPlayer().getBukkit().hasPermission("stratusapi.command.freeze")) {
			String displayName = new SingleAudience(event.getPlayer().getBukkit()).translate("punishment.freeze.item");
			ItemStack iceBlock = new ItemStack(Material.ICE);
			ItemMeta meta = iceBlock.getItemMeta();
			meta.setDisplayName(displayName);
			iceBlock.setItemMeta(meta);
			event.getPlayer().getBukkit().getInventory().setItem(7, iceBlock);
		}
	}

}
