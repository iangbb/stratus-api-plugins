/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.tournament;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import network.stratus.api.tournament.commands.TournamentCommands;

import org.bukkit.plugin.java.JavaPlugin;

public class StratusAPITournament extends JavaPlugin {

	private static StratusAPITournament plugin;

	@Override
	public void onEnable() {
		plugin = this;

		saveDefaultConfig();

		// Set up commands
		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph();
		DispatcherNode root = cmdGraph.getRootDispatcherNode().registerNode("tm");
		root.registerCommands(new TournamentCommands());
		new BukkitIntake(this, cmdGraph).register();

		getLogger().info("Stratus API Tournament extensions enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Tournament extensions disabled");
	}

	public static StratusAPITournament get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

}
