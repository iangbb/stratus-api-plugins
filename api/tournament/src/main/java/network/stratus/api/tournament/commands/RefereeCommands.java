/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.tournament.commands;

import app.ashcon.intake.Command;
import org.bukkit.command.CommandSender;

/**
 * Commands for managing tournament matches.
 *
 * @author Meeples10
 */
public class RefereeCommands {

	@Command(aliases = "register",
			desc = "Register a team for the active match",
			usage = "<team>",
			perms = "stratusapi.tournament.referee.register")
	public void register(CommandSender sender, String teamName) {
		// TODO implement command
	}

	@Command(aliases = "unregister",
			desc = "Unregister a team for the active match",
			usage = "<team>",
			perms = "stratusapi.tournament.referee.unregister")
	public void unregister(CommandSender sender, String teamName) {
		// TODO implement command
	}
}