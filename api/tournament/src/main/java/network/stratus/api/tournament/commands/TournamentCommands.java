/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.tournament.commands;

import app.ashcon.intake.Command;
import org.bukkit.command.CommandSender;

/**
 * General tournament commands not related to team or match management.
 *
 * @author Meeples10
 */
public class TournamentCommands {

	@Command(aliases = "veto",
			desc = "Vote to decide the next map",
			usage = "<class> <first vote> <number of final maps>",
			perms = "stratusapi.tournament.referee.register")
	public void register(CommandSender sender, String mapClass, String teamName, int mapCount) {
		// TODO implement command (see design document for details)
	}
}