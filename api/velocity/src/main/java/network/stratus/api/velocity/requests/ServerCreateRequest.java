/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

import java.util.UUID;

/**
 * A request to create a new server.
 * 
 * @author ShinyDialga
 *
 */
public class ServerCreateRequest implements Request<Void> {

	private String proxy;
	private String name;
	private UUID owner;
	private UUID sender;
	private boolean personal;
	private String size;

	public ServerCreateRequest(String proxy, String name, UUID owner, UUID sender, boolean personal) {
		this(proxy, name, owner, sender, personal, null);
	}

	public ServerCreateRequest(String proxy, String name, UUID owner, UUID sender, boolean personal, String size) {
		this.proxy = proxy;
		this.name = name;
		this.owner = owner;
		this.sender = sender;
		this.personal = personal;
		this.size = size;
	}

	public String getProxy() {
		return proxy;
	}

	public String getName() {
		return name;
	}

	public UUID getOwner() {
		return owner;
	}

	public UUID getSender() {
		return sender;
	}

	public boolean isPersonal() {
		return personal;
	}

	public String getSize() {
		return size;
	}

	@Override
	public String getEndpoint() {
		return "/servers/create";
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.post(this);
	}

}
