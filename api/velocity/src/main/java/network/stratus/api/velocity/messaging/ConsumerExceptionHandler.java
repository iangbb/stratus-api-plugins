/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.messaging;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.impl.DefaultExceptionHandler;

import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Handles an exception thrown by a {@link Consumer}. The exception is logged,
 * and the {@link Channel} remains open.
 * 
 * @author Ian Ballingall
 *
 */
public class ConsumerExceptionHandler extends DefaultExceptionHandler {

	@Override
	public void handleConsumerException(Channel channel, Throwable exception, Consumer consumer, String consumerTag,
			String methodName) {
		StratusAPIVelocity.get().getLogger().error(String.format("Exception occurred in channel %d, tag %s: %s",
				channel.getChannelNumber(), consumerTag, exception.toString()));
		exception.printStackTrace();
	}

}
