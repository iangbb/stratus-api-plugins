/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.messaging;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Encapsulates a player count being sent to or received from other proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class ModifyServer {

	private String proxy;
	private String name;
	private String ip;
	private boolean add;

	public ModifyServer() {

	}

	public ModifyServer(String proxy, String name, String ip, boolean add) {
		this.proxy = proxy;
		this.name = name;
		this.ip = ip;
		this.add = add;
	}

	public String getProxy() {
		return proxy;
	}

	public String getName() {
		return name;
	}

	public String getIp() {
		return ip;
	}

	public boolean isAdd() {
		return add;
	}
}
