/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.listeners;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyPingEvent;
import com.velocitypowered.api.event.query.ProxyQueryEvent;
import com.velocitypowered.api.proxy.server.QueryResponse;
import com.velocitypowered.api.proxy.server.ServerPing;

import network.stratus.api.velocity.proxy.PlayerCountManager;

/**
 * Listens on server list pings and GS4 queries, updating the displayed player
 * count to accordingly show the total across all proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class PingListener {

	private final PlayerCountManager manager;

	public PingListener(PlayerCountManager manager) {
		this.manager = manager;
	}

	@Subscribe
	public void onProxyPing(ProxyPingEvent event) {
		ServerPing.Builder builder = event.getPing().asBuilder();
		builder.onlinePlayers(builder.getOnlinePlayers() + manager.getTotalCount(false));

		if (builder.getOnlinePlayers() >= builder.getMaximumPlayers()) {
			builder.maximumPlayers(builder.getOnlinePlayers() + 1);
		}

		event.setPing(builder.build());
	}

	@Subscribe
	public void onGS4Query(ProxyQueryEvent event) {
		QueryResponse original = event.getResponse();
		QueryResponse.Builder builder = original.toBuilder();
		int currentPlayers = original.getCurrentPlayers() + manager.getTotalCount(false);
		builder.currentPlayers(currentPlayers);

		if (currentPlayers >= original.getMaxPlayers()) {
			builder.maxPlayers(currentPlayers + 1);
		}

		event.setResponse(builder.build());
	}

}
