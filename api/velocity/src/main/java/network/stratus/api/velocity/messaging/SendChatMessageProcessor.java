/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.messaging;

import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.server.ServerInfo;
import net.kyori.text.Component;
import net.kyori.text.TextComponent;
import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.chat.SingleAudience;

import java.net.InetSocketAddress;
import java.util.Optional;

/**
 * Processes a player count message received from other proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class SendChatMessageProcessor implements MessageProcessor<SendChat> {

	public SendChatMessageProcessor() {
	}

	@Override
	public void process(SendChat object) {
		Optional<Player> optional = StratusAPIVelocity.get().getServer().getAllPlayers().stream()
				.filter(p -> p.getUniqueId().equals(object.getUuid()))
				.findFirst();
        if (optional.isPresent()) {
        	Player player = optional.get();

//        	new SingleAudience(player).sendMessage(object.getMessage());
		}
	}

}
