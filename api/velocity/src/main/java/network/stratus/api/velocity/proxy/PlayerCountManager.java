/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.proxy;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

import com.velocitypowered.api.scheduler.ScheduledTask;

import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Manages player counts of other proxies, allowing their total to be obtained.
 * 
 * @author Ian Ballingall
 *
 */
public class PlayerCountManager {

	private Map<String, Integer> playerCounts;
	private Map<String, Long> lastUpdate;

	private @Nullable ScheduledTask publishTask;
	private @Nullable ScheduledTask cleanupTask;

	public PlayerCountManager() {
		this.playerCounts = new ConcurrentHashMap<>();
		this.lastUpdate = new ConcurrentHashMap<>();
	}

	@Nullable
	public ScheduledTask getPublishTask() {
		return publishTask;
	}

	public void setPublishTask(ScheduledTask publishTask) {
		this.publishTask = publishTask;
	}

	@Nullable
	public ScheduledTask getCleanupTask() {
		return cleanupTask;
	}

	public void setCleanupTask(ScheduledTask cleanupTask) {
		this.cleanupTask = cleanupTask;
	}

	public int getPlayerCount(String proxy) {
		return playerCounts.get(proxy);
	}

	public synchronized void setPlayerCount(String proxy, int count) {
		lastUpdate.put(proxy, System.currentTimeMillis());
		playerCounts.put(proxy, count);
	}

	public synchronized void clearPlayerCount(String proxy) {
		lastUpdate.remove(proxy);
		playerCounts.remove(proxy);
	}

	public int getTotalCount(boolean includeThisProxy) {
		return (includeThisProxy ? StratusAPIVelocity.get().getServer().getPlayerCount() : 0)
				+ playerCounts.values().stream().collect(Collectors.summingInt(Integer::intValue));
	}

	/**
	 * Cleanup task which periodically removes proxies who haven't seen sending
	 * updates.
	 */
	public final class Cleanup implements Runnable {

		private final long clearInterval;

		/**
		 * Instantiate a new cleanup task for this manager.
		 * 
		 * @param clearInterval The number of seconds since the last update after which a
		 *                     proxy will be removed from tracking. This should be
		 *                     greater than the publishing interval.
		 */
		public Cleanup(long clearInterval) {
			this.clearInterval = clearInterval;
		}

		@Override
		public void run() {
			final long currentTime = System.currentTimeMillis();
			lastUpdate.entrySet().stream().filter(e -> currentTime - e.getValue() > clearInterval).map(e -> e.getKey())
					.forEach(PlayerCountManager.this::clearPlayerCount);
		}

	}

}
