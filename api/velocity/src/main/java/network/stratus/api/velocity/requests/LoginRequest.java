/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.requests;

import java.util.UUID;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.velocity.responses.LoginResponse;

/**
 * A request triggered with a player attempts to log in. The player's persistent
 * data is updated, and any active ban or IP ban is retrieved. Group and mute
 * information is also available.
 * 
 * @author Ian Ballingall
 *
 */
public class LoginRequest implements Request<LoginResponse> {

	private UUID uuid;
	private String username;
	private String ip;

	public LoginRequest(UUID uuid, String username, String ip) {
		this.uuid = uuid;
		this.username = username;
		this.ip = ip;
	}

	public UUID getUuid() {
		return uuid;
	}

	public String getUsername() {
		return username;
	}

	public String getIp() {
		return ip;
	}

	@Override
	public String getEndpoint() {
		return "/players/login";
	}

	@Override
	public Class<LoginResponse> getResponseType() {
		return LoginResponse.class;
	}

	@Override
	public LoginResponse make(APIClient client) {
		return client.post(this);
	}

}
