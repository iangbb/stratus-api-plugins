/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.chat;

import java.util.Locale;

import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;

import net.kyori.text.TextComponent;
import network.stratus.api.chat.Audience;
import network.stratus.api.i18n.Translation;
import network.stratus.api.velocity.StratusAPIVelocity;

/**
 * Represents an {@link Audience} of one target.
 * 
 * @author Ian Ballingall
 *
 */
public class SingleAudience implements Audience {

	private final CommandSource target;
	private final Locale locale;

	public SingleAudience(CommandSource target) {
		this.target = target;

		// Only players have Locales, so we need to handle this accordingly
		if (target instanceof Player) {
			this.locale = ((Player) target).getPlayerSettings().getLocale();
		} else {
			this.locale = StratusAPIVelocity.get().getTranslator().getDefaultLocale();
		}
	}

	public CommandSource getTarget() {
		return target;
	}

	public Locale getLocale() {
		return locale;
	}

	@Override
	public void sendMessage(Translation translation) {
		target.sendMessage(TextComponent.of(translate(translation)));
	}

	@Override
	public void sendMessage(String stringKey) {
		target.sendMessage(TextComponent.of(translate(stringKey)));
	}

	@Override
	public void sendMessage(String stringKey, Object... args) {
		target.sendMessage(TextComponent.of(translate(stringKey, args)));
	}

	@Override
	public void sendMessageRaw(String message) {
		target.sendMessage(TextComponent.of(message));
	}

	/**
	 * Translates the given {@link Translation} into the target's {@link Locale}.
	 *
	 * @param translation The {@link Translation} containing translation key and
	 *                    values
	 * @return The translated string
	 */
	public String translate(Translation translation) {
		return translation.translate(StratusAPIVelocity.get().getTranslator(), locale);
	}

	/**
	 * Translates the given key into the target's {@link Locale}.
	 *
	 * @param stringKey The translation key
	 * @return The translated string
	 */
	public String translate(String stringKey) {
		return StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(locale, stringKey);
	}

	/**
	 * Translates the given key into the target's {@link Locale} and substitutes in
	 * the arguments.
	 *
	 * @param stringKey The translation key
	 * @param args      The arguments to be substituted
	 * @return The translated string
	 */
	public String translate(String stringKey, Object... args) {
		String message = StratusAPIVelocity.get().getTranslator().getStringOrDefaultLocale(locale, stringKey);
		return String.format(message, args);
	}

	/**
	 * The {@link Audience} consisting solely of the console.
	 */
	public static final SingleAudience CONSOLE = new SingleAudience(
			StratusAPIVelocity.get().getServer().getConsoleCommandSource());

}
