/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.commands;

import com.velocitypowered.api.command.Command;
import com.velocitypowered.api.command.CommandSource;
import com.velocitypowered.api.proxy.Player;
import com.velocitypowered.api.proxy.server.RegisteredServer;
import com.velocitypowered.api.proxy.server.ServerInfo;
import net.kyori.text.TextComponent;
import net.kyori.text.event.ClickEvent;
import network.stratus.api.client.RequestFailureException;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.chat.SingleAudience;
import network.stratus.api.velocity.requests.ProxyCreateRequest;
import network.stratus.api.velocity.requests.ServerCreateRequest;
import org.checkerframework.checker.nullness.qual.NonNull;

import javax.ws.rs.ForbiddenException;
import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * A command to send the player back to the lobby. Presently supports a single
 * lobby server.
 *
 * @author Ian Ballingall
 */
public class ServerCommands {

    public ServerCommands() {
    }

    public static class ServerEditCommand implements Command {
        public ServerEditCommand() {
        }

        @Override
        public void execute(CommandSource source, @NonNull String[] args) {
            //TODO: Allow players to use this?
            if (source instanceof Player) {
                new SingleAudience(source).sendMessage("command.error.player");
                return;
            }

            if (args.length < 4) {
                return;
            }

            boolean add = args[0].equals("add");
            String name = args[1];
            String ip = args[2];
            int port = Integer.parseInt(args[3]);
            ServerInfo info = new ServerInfo(name, new InetSocketAddress(ip, port));

            if (add) {
                StratusAPIVelocity.get().getServer().registerServer(info);
            } else {
                StratusAPIVelocity.get().getServer().unregisterServer(info);
            }

            new SingleAudience(source).sendMessage("proxy.edit.modified");
        }
    }

    public static class PersonalCreateCommand implements Command {
        private Set<UUID> cooldown;

        public PersonalCreateCommand() {
            cooldown = new HashSet<>();
        }

        public final class ServerCommandCooldown implements Runnable {

            private final UUID uuid;

            public ServerCommandCooldown(UUID uuid) {
                this.uuid = uuid;
            }

            @Override
            public void run() {
                cooldown.remove(uuid);
            }
        }

        @Override
        public void execute(CommandSource source, @NonNull String[] args) {
            if (!(source instanceof Player)) {
                new SingleAudience(source).sendMessage("command.error.notplayer");
                return;
            }

            Player player = (Player) source;

            if (cooldown.contains(player.getUniqueId())) {
                new SingleAudience(source).sendMessage("proxy.create.cooldown");
                return;
            }

            cooldown.add(player.getUniqueId());

            StratusAPIVelocity.get().getServer().getScheduler()
                    .buildTask(StratusAPIVelocity.get(),
                            new ServerCommandCooldown(player.getUniqueId()))
                    .delay(5, TimeUnit.SECONDS)
                    .schedule();

            String name = player.getUsername();

            if (!name.matches("[a-zA-Z0-9_]*")) {
                new SingleAudience(source).sendMessage("proxy.create.invalid");
                return;
            }

            name = name.replaceAll("_", "-");

            Optional<RegisteredServer> optionalServer = StratusAPIVelocity.get().getServer().getServer(name);

            optionalServer.ifPresent(server -> {
                if (player.getCurrentServer().isPresent() && player.getCurrentServer().get().getServer().equals(server)) {
                    new SingleAudience(source).sendMessage("proxy.create.alreadyon");
                } else {
                    player.createConnectionRequest(server).fireAndForget();
                }
            });

            if (!optionalServer.isPresent()) {
                try {
                    ServerCreateRequest request = new ServerCreateRequest(StratusAPIVelocity.get().getProxyName(), name, player.getUniqueId(), null, true);
                    request.make(StratusAPIVelocity.get().getApiClient());
                    new SingleAudience(source).sendMessage("proxy.create.success");
                } catch (Exception e) {
                    //TODO: Better handle different kinds of exceptions
                    new SingleAudience(source).sendMessage("proxy.create.permission");
                }
            }
        }
    }

    public static class ServerCreateCommand implements Command {

        public ServerCreateCommand() {

        }

        int MIN_SERVERS = 1;
        int MAX_SERVERS = 50;

        @Override
        public void execute(CommandSource source, @NonNull String[] args) {
            if (!(source instanceof Player)) {
                new SingleAudience(source).sendMessage("command.error.notplayer");
                return;
            }

            if (args.length < 2) {
                return;
            }

            String name = args[0];

            if (!name.matches("[a-zA-Z0-9_]*")) {
                new SingleAudience(source).sendMessage("proxy.create.invalid");
                return;
            }

            name = name.replaceAll("_", "-");

            int min = Math.max(MIN_SERVERS, Integer.parseInt(args[1]));
            int max = Math.min(MAX_SERVERS, args.length > 2 ? Integer.parseInt(args[2]) : min);

            String size = "c-2";
            if (args.length > 3) {
                size = args[3];
            }

            for (int i = min; i <= max; i++) {
                String numberString = (i <= 9 ? "0" : "") + i;
                String currentName = numberString + name;
                Optional<RegisteredServer> optionalServer = StratusAPIVelocity.get().getServer().getServer(currentName);

                Player player = (Player) source;
                optionalServer.ifPresent(server -> {
                    if (player.getCurrentServer().isPresent() && player.getCurrentServer().get().getServer().equals(server)) {
                        new SingleAudience(source).sendMessage("proxy.create.alreadyon");
                    } else {
//                        player.createConnectionRequest(server).fireAndForget();
                    }
                });

                if (!optionalServer.isPresent()) {
                    try {
                        ServerCreateRequest request = new ServerCreateRequest(StratusAPIVelocity.get().getProxyName(), currentName, null, player.getUniqueId(), false, size);
                        request.make(StratusAPIVelocity.get().getApiClient());
                        new SingleAudience(source).sendMessage("proxy.create.success");
                    } catch (Exception e) {
                        new SingleAudience(source).sendMessage("proxy.create.permission");
                    }
                }
            }
        }
    }

    public static class ProxyCreateCommand implements Command {

        public ProxyCreateCommand() {

        }

        @Override
        public void execute(CommandSource source, @NonNull String[] args) {
            String name = args[0];

            if (!name.matches("[a-zA-Z0-9_]*")) {
                new SingleAudience(source).sendMessage("proxy.create.invalid");
                return;
            }

            name = name.replaceAll("_", "-");

            String size = "c-2";
            if (args.length > 1) {
                size = args[1];
            }

            UUID sender = source instanceof Player ? ((Player)source).getUniqueId() : null;

            try {
                ProxyCreateRequest request = new ProxyCreateRequest(name, sender, size);
                request.make(StratusAPIVelocity.get().getApiClient());
                new SingleAudience(source).sendMessage("proxy.create.success");
            } catch (RequestFailureException e) {
                if (e.getResponse().isPresent()) {
                    source.sendMessage(TextComponent.of(e.getResponse().get().getDescription()));
                } else {
                    new SingleAudience(source).sendMessage("proxy.create.permission");
                }
            }
        }
    }

    public static class RankedCommand implements Command {

        public RankedCommand() {

        }
        @Override
        public void execute(CommandSource source, @NonNull String[] args) {
            if (!(source instanceof Player)) {
                new SingleAudience(source).sendMessage("command.error.notplayer");
                return;
            }

            Player player = (Player) source;

            Optional<RegisteredServer> optionalServer = StratusAPIVelocity.get().getServer().getServer("Ranked");

            optionalServer.ifPresent(server -> {
                if (player.getCurrentServer().isPresent() && player.getCurrentServer().get().getServer().equals(server)) {
                    new SingleAudience(source).sendMessage("proxy.create.alreadyon");
                } else {
                    player.createConnectionRequest(server).fireAndForget();
                }
            });
        }
    }
}
