/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.models.punishments;

import network.stratus.api.models.punishment.Punishment;
import network.stratus.api.models.punishment.PunishmentFactory;

/**
 * Manufactures the appropriate {@link Punishment} type for a Velocity proxy.
 * Presently, this only supports {@link PunishmentFactory.Type}{@code .BAN}, which
 * produces a {@link Ban}.
 * 
 * @author Ian Ballingall
 *
 */
public class VelocityPunishmentFactory extends PunishmentFactory {

	@Override
	public Punishment getObject() {
		if (type == Type.BAN) {
			return new Ban(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
		} else {
			throw new IllegalArgumentException("Cannot instantiate punishment type: " + type.toString());
		}
	}

}
