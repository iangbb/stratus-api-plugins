/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.velocity.tasks;

import java.io.IOException;

import com.velocitypowered.api.proxy.server.RegisteredServer;
import network.stratus.api.messaging.Publisher;
import network.stratus.api.velocity.StratusAPIVelocity;
import network.stratus.api.velocity.messaging.PlayerCount;
import network.stratus.api.velocity.messaging.ServerPlayerCount;

/**
 * Broadcasts the player count periodically to all other proxies.
 * 
 * @author Ian Ballingall
 *
 */
public class PlayerCountBroadcastTask implements Runnable {

	private final StratusAPIVelocity plugin;
	private final String proxyName;
	private final Publisher publisher;
	private final Publisher serverPublisher;

	/** Used to track if empty at previous run. */
	private boolean empty = false;

	public PlayerCountBroadcastTask(StratusAPIVelocity plugin, String proxyName, Publisher publisher, Publisher serverPublisher) {
		this.plugin = plugin;
		this.proxyName = proxyName;
		this.publisher = publisher;
		this.serverPublisher = serverPublisher;
	}

	@Override
	public void run() {
		final int players = plugin.getServer().getPlayerCount();

		// Don't bother broadcasting if the proxy is continuously empty - only the first
		// time the proxy drops to 0 players
		if (players > 0 || !empty) {
			try {
				publisher.publish(new PlayerCount(proxyName, players));
				for (RegisteredServer server : plugin.getServer().getAllServers()) {
					serverPublisher.publish(new ServerPlayerCount(proxyName, server.getServerInfo().getName(), server.getPlayersConnected().size()));
				}
			} catch (IOException e) {
				plugin.getLogger().error("Failed to publish player count: " + e);
				e.printStackTrace();
			}
		}

		empty = players == 0;
	}

}
