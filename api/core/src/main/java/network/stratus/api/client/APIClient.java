/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.client;

/**
 * Represents a client which connects to a REST API and performs HTTP requests
 * to retrieve, store and update data.
 * 
 * @author Ian Ballingall
 *
 */
public interface APIClient {

	/**
	 * Obtain the base URL used by this API client.
	 * 
	 * @return The base URL
	 */
	public String getUrl();

	/**
	 * Perform a GET request.
	 * 
	 * @param <T>     The type of object to be returned by the API
	 * @param request The request object
	 * @return The deserialised response from the server
	 * @throws RequestFailureException If the request fails to complete
	 */
	public <T> T get(Request<T> request) throws RequestFailureException;

	/**
	 * Perform a POST request.
	 * 
	 * @param <T>     The type of object to be returned by the API
	 * @param request The request object
	 * @return The deserialised response from the server
	 * @throws RequestFailureException If the request fails to complete
	 */
	public <T> T post(Request<T> request) throws RequestFailureException;

	/**
	 * Perform a PUT request.
	 * 
	 * @param <T>     The type of object to be returned by the API
	 * @param request The request object
	 * @return The deserialised response from the server
	 * @throws RequestFailureException If the request fails to complete
	 */
	public <T> T put(Request<T> request) throws RequestFailureException;

	/**
	 * Perform a DELETE request.
	 * 
	 * @param <T>     The type of object to be returned by the API
	 * @param request The request object
	 * @return The deserialised response from the server
	 * @throws RequestFailureException If the request fails to complete
	 */
	public <T> T delete(Request<T> request) throws RequestFailureException;

}
