/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.util;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Class containing common utilities.
 * 
 * @author Ian Ballingall
 *
 */
public class Util {

	/**
	 * Construct a comma-separated list of string values for a query parameter. If
	 * no values are provided, then the empty string is returned.
	 * 
	 * @param values The list of values to convert join
	 * @return The comma-separated list of values
	 */
	public static String buildPropertiesListString(List<?> values) {
		if (values == null || values.isEmpty())
			return "";

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < values.size() - 1; i++) {
			sb.append(values.get(i).toString()).append(",");
		}
		sb.append(values.get(values.size() - 1));
		return sb.toString();
	}

}
