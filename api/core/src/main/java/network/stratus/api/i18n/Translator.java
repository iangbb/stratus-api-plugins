/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Locale;
import java.util.Optional;

/**
 * Translates strings into the appropriate language.
 * 
 * @author Ian Ballingall
 *
 */
public interface Translator {

	/**
	 * Determines if a {@link Locale} has the given string.
	 * 
	 * @param locale The locale to check
	 * @param key    The string to check
	 * @return Whether this locale defines this string
	 */
	public boolean hasString(Locale locale, String key);

	/**
	 * Get the string corresponding to the given {@link Locale}.
	 * 
	 * @param locale The locale to get
	 * @param key    The string to get
	 * @return The string translated into this locale
	 */
	public Optional<String> getString(Locale locale, String key);

	/**
	 * Get the string in the given {@link Locale}, or in the default {@link Locale} if it does not
	 * exist.
	 * 
	 * @param locale The locale to get
	 * @param key    The string to get
	 * @return The string translated into this locale, if it exists, or in the
	 *         default locale otherwise
	 */
	public String getStringOrDefaultLocale(Locale locale, String key);

	/**
	 * Get the string in the given {@link Locale}, or return the given value if it does not
	 * exist.
	 * 
	 * @param locale        The locale to get
	 * @param key           The string to get
	 * @param defaultOption The string to return if the key is not defined in the
	 *                      locale
	 * @return The string translated into this locale, or the default
	 */
	public String getStringOrDefault(Locale locale, String key, String defaultOption);

	/**
	 * Get the default {@link Locale} for this translator.
	 * 
	 * @return The default locale
	 */
	public Locale getDefaultLocale();

	/**
	 * Generates a string which indicates the difference in time between two {@link Date}s.
	 * 
	 * @param locale The locale to get
	 * @param start  The start date
	 * @param end    The end date
	 * @return A string indicating the difference
	 */
	default public String getTimeDifferenceString(Locale locale, Date start, Date end) {
		if (start == null) {
			return getStringOrDefaultLocale(locale, "time.null.start");
		}

		if (end == null) {
			return getStringOrDefaultLocale(locale, "time.null.end");
		}

		String result = "";
		LocalDateTime startDate = LocalDateTime.ofInstant(start.toInstant(), ZoneId.systemDefault());
		LocalDateTime endDate = LocalDateTime.ofInstant(end.toInstant(), ZoneId.systemDefault());

		long difference;
		if ((difference = Math.abs(ChronoUnit.YEARS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.year.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.year"), difference);
		} else if ((difference = Math.abs(ChronoUnit.MONTHS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.month.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.month"), difference);
		} else if ((difference = Math.abs(ChronoUnit.WEEKS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.week.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.week"), difference);
		} else if ((difference = Math.abs(ChronoUnit.DAYS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.day.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.day"), difference);
		} else if ((difference = Math.abs(ChronoUnit.HOURS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.hour.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.hour"), difference);
		} else if ((difference = Math.abs(ChronoUnit.MINUTES.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.minute.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.minute"), difference);
		} else if ((difference = Math.abs(ChronoUnit.SECONDS.between(startDate, endDate))) > 0) {
			result = (difference > 1)
					? String.format(getStringOrDefaultLocale(locale, "time.difference.second.plural"), difference)
					: String.format(getStringOrDefaultLocale(locale, "time.difference.second"), difference);
		} else {
			result = getStringOrDefaultLocale(locale, "time.difference.moments");
		}

		if (startDate.compareTo(endDate) <= 0) {
			result = String.format(getStringOrDefaultLocale(locale, "time.past"), result);
		} else {
			result = String.format(getStringOrDefaultLocale(locale, "time.future"), result);
		}

		return result;
	}

}
