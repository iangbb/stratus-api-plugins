/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;
import network.stratus.api.permissions.Group;

/**
 * Represents a request to the API for {@link Group} information, using the
 * group's unique name.
 * 
 * @author Ian Ballingall
 *
 */
public class GroupNameRequest implements Request<Group> {

	private String name;

	public GroupNameRequest(String name) {
		this.name = name;
	}

	@Override
	public String getEndpoint() {
		return "/permissions/groups/name/" + name;
	}

	@Override
	public Class<Group> getResponseType() {
		return Group.class;
	}

	@Override
	public Group make(APIClient client) {
		return client.get(this);
	}

}
