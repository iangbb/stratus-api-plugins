/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models;

import java.util.Date;

/**
 * Represents an individual IP ban.
 * 
 * @author Ian Ballingall
 *
 */
public class IpBan {

	/** The object ID for this ban, to allow removal. */
	private String _id;
	/** The specific IP, range or CIDR banned. */
	private String bannedIp;
	/** The description for this ban. */
	private String description;
	/** When this ban was issued. */
	private Date time;

	public String get_id() {
		return _id;
	}

	public String getBannedIp() {
		return bannedIp;
	}

	public String getDescription() {
		return description;
	}

	public Date getTime() {
		return time;
	}


}
