/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.i18n;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.Properties;

/**
 * Manages {@link Language}s and {@link Locale}s.
 * 
 * @author Ian Ballingall
 *
 */
public class LanguageManager {

	/** The default locale, which can be used if a language is missing a string. */
	private Locale defaultLocale;
	/** A map of locales to their languages. */
	private Map<Locale, Language> languageMap;

	public LanguageManager(Locale defaultLocale, Map<Locale, Language> languageMap) {
		this.defaultLocale = defaultLocale;
		this.languageMap = languageMap;
	}

	/**
	 * Get the default locale.
	 * 
	 * @return The default locale
	 */
	public Locale getDefaultLocale() {
		return defaultLocale;
	}

	/**
	 * Get the friendly display name for the default locale.
	 * 
	 * @return The default locale's display name
	 */
	public String getDefaultLocaleDisplayName() {
		return defaultLocale.getDisplayName();
	}

	/**
	 * Determine if this language is defined.
	 * 
	 * @param locale The locale to check
	 * @return If a language exists for this locale
	 */
	public boolean hasLanguage(Locale locale) {
		return languageMap.containsKey(locale);
	}

	/**
	 * Determine if this language is defined.
	 * 
	 * @param languageCode The two letter language code
	 * @param countryCode  The two letter country code
	 * @return If a language exists for this locale
	 */
	public boolean hasLanguage(String languageCode, String countryCode) {
		return hasLanguage(new Locale(languageCode, countryCode));
	}

	/**
	 * Get the language corresponding to the given locale.
	 * 
	 * @param locale The locale to get
	 * @return The language corresponding to this locale
	 */
	public Optional<Language> getLanguage(Locale locale) {
		return Optional.ofNullable(languageMap.get(locale));
	}

	/**
	 * Get the language corresponding to the given locale.
	 * 
	 * @param languageCode The two letter language code
	 * @param countryCode  The two letter country code
	 * @return The language corresponding to this locale
	 */
	public Optional<Language> getLanguage(String languageCode, String countryCode) {
		return getLanguage(new Locale(languageCode, countryCode));
	}

	/**
	 * Get the default language: the language corresponding to the default locale.
	 * 
	 * @return The default language
	 */
	public Language getDefaultLanguage() {
		return getLanguage(defaultLocale).get();
	}

	/**
	 * Builds a new {@link LanguageManager} based on a file mapping strings to their
	 * values.
	 */
	public static class Builder {

		/** The default locale for the language manager. */
		private Locale defaultLocale;
		/** The list of language files. */
		private File[] languageFiles;
		/** The map of locales, as strings, to their properties. */
		private Map<String, Properties> languageProperties;

		public Builder(Locale defaultLocale) {
			this.defaultLocale = defaultLocale;
		}

		/**
		 * Loads the language files from a directory within the JAR.
		 * 
		 * @param directory The internal directory
		 * @return The builder
		 */
		public Builder loadInternally(String directory) {
			// TODO: allow loading internally
			return this;
		}

		/**
		 * Loads the languages files from a directory outside of the JAR.
		 * 
		 * @param directory The external directory
		 * @return The builder
		 */
		public Builder loadExternally(String directory) {
			this.languageFiles = new File(directory).listFiles(new LanguagePropertiesFileFilter());
			return this;
		}

		/**
		 * Loads the property values from the files into the map.
		 * 
		 * @return The builder
		 * @throws FileNotFoundException If a properties file is not found
		 * @throws IOException           If an error occurs when reading a file
		 */
		public Builder loadProperties() throws FileNotFoundException, IOException {
			this.languageProperties = new HashMap<>();

			for (File file : languageFiles) {
				Properties properties = new Properties();
				properties.load(new FileInputStream(file));
				languageProperties.put(file.getName().substring(0, 5), properties);
			}

			return this;
		}

		/**
		 * Builds the {@link LanguageManager} object from the language properties.
		 * 
		 * @return The {@link LanguageManager}
		 */
		public LanguageManager build() {
			Map<Locale, Language> map = new HashMap<>();
			languageProperties.forEach((locale, properties) -> {
				Locale l = new Locale(locale.substring(0, 2), locale.substring(3, 5));
				Map<String, String> strings = new HashMap<>();
				for (String key : properties.stringPropertyNames()) {
					strings.put(key.intern(), properties.getProperty(key).replace('`', '\u00A7'));
				}
				map.put(l, new Language(l, strings));
			});

			return new LanguageManager(defaultLocale, map);
		}

	}

}
