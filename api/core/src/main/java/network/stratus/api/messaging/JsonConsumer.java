/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.messaging;

import java.io.IOException;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

/**
 * Consumes an item represented in JSON, deserialises it into a POJO and
 * processes it.
 * 
 * @author Ian Ballingall
 *
 * @param <T> The type to deserialise the JSON into
 */
public class JsonConsumer<T> extends DefaultConsumer {

	private Class<T> type;
	private MessageProcessor<T> processor;
	private boolean autoAck;

	public JsonConsumer(Channel channel, Class<T> type, MessageProcessor<T> processor, boolean autoAck) {
		super(channel);
		this.type = type;
		this.processor = processor;
		this.autoAck = autoAck;
	}

	@Override
	public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
			throws JsonParseException, JsonMappingException, IOException {
		String contentType = properties.getContentType();
		long deliveryTag = envelope.getDeliveryTag();

		if (contentType.equals("application/json")) {
			String json = new String(body, properties.getContentEncoding());
			T resultObject = new ObjectMapper().readValue(json, type);
			processor.process(resultObject);
		} else {
			throw new IllegalArgumentException("JSON consumer was given invalid content type: " + contentType);
		}

		if (!autoAck) {
			getChannel().basicAck(deliveryTag, false);
		}
	}

	/**
	 * Builds a {@link JsonConsumer} and registers it to a channel.
	 * 
	 * @author Ian Ballingall
	 *
	 * @param <T> The type to deserialise the JSON into
	 */
	public static class Builder<T> {

		private Channel channel;
		private String consumerName;
		private String exchangeName = "";
		private String exchangeType = BuiltinExchangeType.DIRECT.getType();
		private String routingKey = "json";
		private Class<T> messageType;
		private MessageProcessor<T> processor = new NullMessageProcessor<T>();
		private boolean autoAck = true;

		private JsonConsumer<T> result;

		/**
		 * Instantiate a new builder with the required fields.
		 * 
		 * @param channel     The channel the consumer will operate on
		 * @param messageType The class type corresponding to T
		 */
		public Builder(Channel channel, Class<T> messageType) {
			this.channel = channel;
			this.messageType = messageType;
		}

		/**
		 * The internal friendly name which will be attached to this consumer.
		 * 
		 * @param name The consumer name
		 * @return The builder
		 */
		public Builder<T> consumerName(String name) {
			this.consumerName = name;
			return this;
		}

		/**
		 * The name of the exchange messages will be consumed from. This defaults to an
		 * empty string, the default exchange.
		 * 
		 * @param name The exhange name
		 * @return The builder
		 */
		public Builder<T> exchangeName(String name) {
			this.exchangeName = name;
			return this;
		}

		/**
		 * The type of exchange which will be used. This defaults to
		 * {@link BuiltinExchangeType}{@code .DIRECT}.
		 * 
		 * @param type The exchange type
		 * @return The builder
		 */
		public Builder<T> exchangeType(BuiltinExchangeType type) {
			this.exchangeType = type.getType();
			return this;
		}

		/**
		 * The type of exchange which will be used. This defaults to
		 * {@link BuiltinExchangeType}{@code .DIRECT}.
		 * 
		 * @param type The exchange type
		 * @return The builder
		 */
		public Builder<T> exchangeType(String type) {
			this.exchangeType = type;
			return this;
		}

		/**
		 * The routing key, or binding key. Defaults to "json". ".json" is automatically
		 * appended to this key.
		 * 
		 * @param key The routing key
		 * @return The builder
		 */
		public Builder<T> routingKey(String key) {
			this.routingKey = key;
			return this;
		}

		/**
		 * The message processor called after deserialising messages. This defaults to
		 * {@link NullMessageProcessor}, which does nothing.
		 * 
		 * @param processor The message processor
		 * @return The builder
		 */
		public Builder<T> messageProcessor(MessageProcessor<T> processor) {
			this.processor = processor;
			return this;
		}

		/**
		 * Whether or not to automatically acknowledge messages. Defaults to true.
		 * 
		 * @param autoAck The automatic acknowledgement setting
		 * @return The builder
		 */
		public Builder<T> autoAck(boolean autoAck) {
			this.autoAck = autoAck;
			return this;
		}

		/**
		 * Builds a {@link JsonConsumer} based on the parameters provided.
		 * 
		 * @return The builder
		 */
		public Builder<T> build() {
			result = new JsonConsumer<T>(channel, messageType, processor, autoAck);
			return this;
		}

		/**
		 * Get the resulting {@link JsonConsumer} after building.
		 * 
		 * @return The consumer
		 */
		public JsonConsumer<T> getResult() {
			return result;
		}

		/**
		 * Register the resulting {@link JsonConsumer} to the given {@link Channel},
		 * based on the parameters provided.
		 * 
		 * @throws IOException
		 */
		public void register() throws IOException {
			channel.exchangeDeclare(exchangeName, exchangeType);
			String queueName = channel.queueDeclare().getQueue();
			channel.queueBind(queueName, exchangeName, routingKey);
			channel.basicConsume(queueName, autoAck, consumerName, result);
		}

	}

}
