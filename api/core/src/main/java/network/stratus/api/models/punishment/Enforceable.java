/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.punishment;

/**
 * Represents an enforceable punishment against a player.
 * 
 * @author Ian Ballingall
 *
 */
public interface Enforceable {

	/**
	 * Enforce this punishment, announcing the punishment in chat to the relevant
	 * parties, and applying it to the target if they are online. The server will
	 * not be shown.
	 */
	default void enforce() {
		enforce(false);
	}

	/**
	 * Enforce this punishment, announcing the punishment in chat to the relevant
	 * parties, and applying it to the target if they are online.
	 *
	 * @param showServer Whether to include the source server name
	 */
	void enforce(boolean showServer);

}
