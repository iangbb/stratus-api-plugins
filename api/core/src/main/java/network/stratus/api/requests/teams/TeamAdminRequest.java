/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.requests.teams;

import network.stratus.api.client.APIClient;
import network.stratus.api.client.Request;

/**
 * A request relating to administration of teams and its members. Supports
 * adding and remove members and changing the leader.
 * 
 * @author Ian Ballingall
 *
 */
public class TeamAdminRequest implements Request<Void> {

	private String endpoint;
	private String teamName;
	private String target;

	private TeamAdminRequest(String endpoint, String teamName, String target) {
		this.endpoint = endpoint;
		this.teamName = teamName;
		this.target = target;
	}

	public String getTeamName() {
		return teamName;
	}

	public String getTarget() {
		return target;
	}

	@Override
	public String getEndpoint() {
		return endpoint;
	}

	@Override
	public Class<Void> getResponseType() {
		return Void.class;
	}

	@Override
	public Void make(APIClient client) {
		return client.post(this);
	}

	/**
	 * Add a player to a team.
	 * 
	 * @param teamName The name of the target team
	 * @param target   The username of the target player
	 * @return The request object
	 */
	public static TeamAdminRequest addPlayer(String teamName, String target) {
		return new TeamAdminRequest("/teams/admin/add", teamName, target);
	}

	/**
	 * Remove a player from a team.
	 * 
	 * @param teamName The name of the target team
	 * @param target   The username of the target player
	 * @return The request object
	 */
	public static TeamAdminRequest removePlayer(String teamName, String target) {
		return new TeamAdminRequest("/teams/admin/remove", teamName, target);
	}

	/**
	 * Promote a player to team leader.
	 * 
	 * @param teamName The name of the target team
	 * @param target   The username of the target player
	 * @return The request object
	 */
	public static TeamAdminRequest promotePlayer(String teamName, String target) {
		return new TeamAdminRequest("/teams/admin/promote", teamName, target);
	}

}
