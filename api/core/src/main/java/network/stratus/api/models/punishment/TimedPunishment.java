/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.models.punishment;

import java.util.Date;
import network.stratus.api.models.User;

/**
 * Abstract representation of a {@link Punishment} which has both a fixed-date
 * expiry, and an expiry after certain amount of gameplay time has elapsed.
 * 
 * @author Ian Ballingall
 *
 */
public abstract class TimedPunishment extends Punishment {

	protected long timeRemaining;

	public TimedPunishment(String _id, User issuer, User target, Date time, Date expiry, String reason, boolean active,
			int number, String serverName, long timeRemaining, boolean silent) {
		super(_id, issuer, target, time, expiry, reason, active, number, serverName, silent);
		this.timeRemaining = timeRemaining;
	}

	public long getTimeRemaining() {
		return timeRemaining;
	}

	public void setTimeRemaining(long timeRemaining) {
		this.timeRemaining = timeRemaining;
	}

	@Override
	public boolean hasExpired() {
		return super.hasExpired() || timeRemaining <= 0;
	}

}
