/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.messaging;

import com.google.common.collect.Iterators;
import network.stratus.api.bukkit.StratusAPI;
import network.stratus.api.messaging.MessageProcessor;
import network.stratus.api.ranked.StratusAPIRanked;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.countdowns.SingleCountdownContext;
import tc.oc.pgm.restart.RestartCountdown;

import java.io.IOException;

/**
 * Starts a ranked match
 * 
 * @author ShinyDialga
 *
 */
public class FindServerMessageProcessor implements MessageProcessor<FindRankedServer> {

	public FindServerMessageProcessor() {
	}

	@Override
	public void process(FindRankedServer object) {
		if (!object.getProxy().equalsIgnoreCase(StratusAPIRanked.get().getProxy())) {
			return;
		}

		if (StratusAPIRanked.get().getCurrentRankedMatch() == null) {
			Match currentMatch = Iterators.getNext(PGM.get().getMatchManager().getMatches(), null);
			if (currentMatch != null) {
				SingleCountdownContext ctx = (SingleCountdownContext) currentMatch.getCountdown();
				if (ctx.getCountdown(RestartCountdown.class) != null) {
					return;
				}

				if (currentMatch.isRunning()) {
					return;
				}

				if (currentMatch.getParticipants().size() > 0) {
					return;
				}
			}

			try {
				StratusAPIRanked.get().getFoundRankedServerPublisher().publish(new FoundRankedServer(StratusAPIRanked.get().getServerName(), StratusAPIRanked.get().getProxy()));
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
