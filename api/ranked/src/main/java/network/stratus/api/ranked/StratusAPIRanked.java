/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked;

import app.ashcon.intake.bukkit.BukkitIntake;
import app.ashcon.intake.bukkit.graph.BasicBukkitCommandGraph;
import app.ashcon.intake.fluent.DispatcherNode;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.commands.modules.StratusBukkitCommandModule;
import network.stratus.api.bukkit.messaging.ConsumerExceptionHandler;
import network.stratus.api.messaging.JsonConsumer;
import network.stratus.api.messaging.JsonPublisher;
import network.stratus.api.messaging.RabbitMessenger;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.commands.ParticipantCommands;
import network.stratus.api.pgm.match.DefaultReadyManager;
import network.stratus.api.ranked.commands.RankedCommands;
import network.stratus.api.ranked.commands.ReadyCommands;
import network.stratus.api.ranked.listeners.RankedListener;
import network.stratus.api.ranked.match.ParticipateInMatchManager;
import network.stratus.api.ranked.match.RankedMatchListener;
import network.stratus.api.ranked.messaging.FindRankedServer;
import network.stratus.api.ranked.messaging.FindServerMessageProcessor;
import network.stratus.api.ranked.messaging.RankedMatch;
import network.stratus.api.ranked.messaging.RankedMatchMessageProcessor;
import org.bukkit.plugin.java.JavaPlugin;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeoutException;

/**
 * Extensions to the Stratus API for Ranked gameplay. This requires PGM
 * extensions. Provides participant management implementations for Ranked.
 * 
 * @author Ian Ballingall
 *
 */
public class StratusAPIRanked extends JavaPlugin {

	private static int FORFEIT_TIME = 120;

	private static StratusAPIRanked plugin;

	private RabbitMessenger messenger;
	private JsonPublisher foundRankedServerPublisher;
	private JsonPublisher matchFinishPublisher;

	private String rankedDiscord;
	private Match currentMatch;
	private RankedMatch currentRankedMatch;
	private Set<UUID> currentlyDisconnected;
	private Map<UUID, Integer> disconnectTime;
	private Map<UUID, String> disconnectUsernames;
	private Set<UUID> forfeits;
	private String proxy;
	private String serverName;

	private boolean ready;

	@Override
	public void onEnable() {
		plugin = this;

		ready = false;

		createDisconnectedTask();

		saveDefaultConfig();

		getServer().getPluginManager().registerEvents(new RankedListener(), this);

		ParticipateInMatchManager participantManager = new ParticipateInMatchManager(this);
		StratusAPIPGM.get().setParticipantManager(participantManager);
		getServer().getPluginManager().registerEvents(new RankedMatchListener(getConfig().getString("discordUrl", "")),
				this);

		StratusAPIPGM.get().setReadyManager(new DefaultReadyManager());

		BasicBukkitCommandGraph cmdGraph = new BasicBukkitCommandGraph(new StratusBukkitCommandModule());
		DispatcherNode root = cmdGraph.getRootDispatcherNode();
		root.registerCommands(new RankedCommands());
		root.registerCommands(new ReadyCommands());
		root.registerNode("participant").registerCommands(new ParticipantCommands());
		new BukkitIntake(this, cmdGraph).register();

		if (getConfig().getBoolean("messaging.enabled", true)) {
			messenger = new RabbitMessenger(getConfig().getString("messaging.uri"), new ConsumerExceptionHandler());
			try {
				messenger.initialiseConnection();
			} catch (KeyManagementException | NoSuchAlgorithmException | URISyntaxException | IOException
					| TimeoutException | UnrecoverableKeyException | KeyStoreException | CertificateException e) {
				getLogger().severe("Failed to connect to RabbitMQ server: " + e);
				getLogger().severe("Messaging services will be disabled");
				e.printStackTrace();
				messenger = null;
			}

			try {
				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("foundrankedserver", BuiltinExchangeType.TOPIC);
				foundRankedServerPublisher = new JsonPublisher(publisherChannel, "foundrankedserver", "json",
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to enable publisher: " + e);
			}

			try {
				Channel publisherChannel = messenger.getNewChannel();
				publisherChannel.exchangeDeclare("matchfinish", BuiltinExchangeType.TOPIC);
				matchFinishPublisher = new JsonPublisher(publisherChannel, "matchfinish", "json",
						new AMQP.BasicProperties.Builder().contentType("application/json").contentEncoding("UTF-8")
								.deliveryMode(1).build());
			} catch (IOException e) {
				getLogger().severe("Failed to enable publisher: " + e);
			}

			try {
				new JsonConsumer.Builder<FindRankedServer>(messenger.getNewChannel(), FindRankedServer.class)
						.consumerName("findrankedserver").exchangeName("findrankedserver").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey("json").messageProcessor(new FindServerMessageProcessor()).build()
						.register();
			} catch (IOException e) {
				getLogger().severe("Failed to enable processor: " + e);
			}

			try {
				new JsonConsumer.Builder<RankedMatch>(messenger.getNewChannel(), RankedMatch.class)
						.consumerName("rankedmatch").exchangeName("rankedmatch").exchangeType(BuiltinExchangeType.TOPIC)
						.routingKey("json").messageProcessor(new RankedMatchMessageProcessor()).build()
						.register();
			} catch (IOException e) {
				getLogger().severe("Failed to enable processor: " + e);
			}
		}

		this.rankedDiscord = getConfig().getString("rankedDiscord", "");
		this.proxy = getConfig().getString("proxy", "");
		this.serverName = getConfig().getString("serverName", "");

		getLogger().info("Stratus API Ranked extensions enabled");
	}

	@Override
	public void onDisable() {
		plugin = null;
		getLogger().info("Stratus API Ranked extensions disabled");
	}

	/**
	 * Get the current Stratus API plugin instance.
	 * 
	 * @return The plugin instance object
	 */
	public static StratusAPIRanked get() {
		if (plugin == null)
			throw new IllegalStateException("Plugin is not enabled");

		return plugin;
	}

	public String getServerName() {
		return serverName;
	}

	public String getRankedDiscord() {
		return rankedDiscord;
	}

	public Match getCurrentMatch() {
		return currentMatch;
	}

	public void setCurrentMatch(Match currentMatch) {
		this.currentMatch = currentMatch;
	}

	public RankedMatch getCurrentRankedMatch() {
		return currentRankedMatch;
	}

	public void setCurrentRankedMatch(RankedMatch currentRankedMatch) {
		this.currentRankedMatch = currentRankedMatch;
	}

	public JsonPublisher getFoundRankedServerPublisher() {
		return foundRankedServerPublisher;
	}

	public JsonPublisher getMatchFinishPublisher() {
		return matchFinishPublisher;
	}

	public Map<UUID, Integer> getDisconnectTime() {
		return disconnectTime;
	}

	public Set<UUID> getCurrentlyDisconnected() {
		return currentlyDisconnected;
	}

	public Map<UUID, String> getDisconnectUsernames() {
		return disconnectUsernames;
	}

	public Set<UUID> getForfeits() {
		return forfeits;
	}

	public String getProxy() {
		return proxy;
	}

	public boolean isReady() {
		return ready;
	}

	public void setReady(boolean ready) {
		this.ready = ready;
	}

	private void createDisconnectedTask() {
		currentlyDisconnected = new HashSet<>();
		forfeits = new HashSet<>();
		disconnectTime = new HashMap<>();
		disconnectUsernames = new HashMap<>();
		getServer().getScheduler().runTaskTimer(this, () -> {
			currentlyDisconnected.forEach(uuid -> {
				int newTime = disconnectTime.getOrDefault(uuid, 0) + 1;
				disconnectTime.put(uuid, newTime);
				if (newTime >= FORFEIT_TIME) {
					if (!StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().getRankedForfeits().contains(uuid)) {
						StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().getRankedForfeits().add(uuid);
						if (disconnectUsernames.containsKey(uuid)) {
							new MultiAudience.Builder().global().build().sendMessage("ranked.abandon", disconnectUsernames.get(uuid));
						} else {
							new MultiAudience.Builder().global().build().sendMessage("ranked.abandon.unknown");
						}

						new MultiAudience.Builder().global().build().sendMessage("ranked.abandon.forfeit");
					}
				}
			});
		}, 0, 20L);
	}
}
