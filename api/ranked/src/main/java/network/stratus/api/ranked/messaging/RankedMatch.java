/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.messaging;

import java.util.List;

public class RankedMatch {

	private String rankedId;
	private String proxy;
	private String server;
	private String map;
	private boolean premium;
	private List<RankedPlayer> team1;
	private List<RankedPlayer> team2;
	private String channel1Id;
	private String channel2Id;

	public RankedMatch() {

	}

	public RankedMatch(String rankedId, String proxy, String server, String map, boolean premium, List<RankedPlayer> team1, List<RankedPlayer> team2, String channel1Id, String channel2Id) {
		this.rankedId = rankedId;
		this.proxy = proxy;
		this.server = server;
		this.map = map;
		this.premium = premium;
		this.team1 = team1;
		this.team2 = team2;
		this.channel1Id = channel1Id;
		this.channel2Id = channel2Id;
	}

	public String getRankedId() {
		return rankedId;
	}

	public String getProxy() {
		return proxy;
	}

	public String getServer() {
		return server;
	}

	public String getMap() {
		return map;
	}

	public boolean isPremium() {
		return premium;
	}

	public List<RankedPlayer> getTeam1() {
		return team1;
	}

	public List<RankedPlayer> getTeam2() {
		return team2;
	}

	public String getChannel1Id() {
		return channel1Id;
	}

	public String getChannel2Id() {
		return channel2Id;
	}
}

