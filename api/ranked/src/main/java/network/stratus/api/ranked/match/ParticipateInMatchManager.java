/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.match;

import network.stratus.api.pgm.match.UuidMappedParticipantManager;
import network.stratus.api.ranked.StratusAPIRanked;

/**
 * Manages participants based on who the participants are at
 * {@link tc.oc.pgm.api.match.Match} start. Uses a {@link ParticipationListener}
 * to perform this function.
 * 
 * @author Ian Ballingall
 *
 */
public class ParticipateInMatchManager extends UuidMappedParticipantManager {

	public ParticipateInMatchManager(StratusAPIRanked plugin) {
		super();
		plugin.getServer().getPluginManager().registerEvents(new ParticipationListener(this), plugin);
	}

}
