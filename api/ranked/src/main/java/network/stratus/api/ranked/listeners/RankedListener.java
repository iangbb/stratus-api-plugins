/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.listeners;

import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.ranked.StratusAPIRanked;
import network.stratus.api.ranked.messaging.MatchFinish;
import network.stratus.api.ranked.messaging.RankedMatch;
import network.stratus.api.ranked.messaging.RankedPlayer;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.event.MatchFinishEvent;
import tc.oc.pgm.api.match.event.MatchLoadEvent;
import tc.oc.pgm.api.match.event.MatchStartEvent;
import tc.oc.pgm.cycle.CycleMatchModule;
import tc.oc.pgm.teams.Team;
import tc.oc.pgm.teams.TeamMatchModule;

import java.io.IOException;
import java.time.Duration;
import java.util.Iterator;
import java.util.UUID;

/**
 * Listener methods pertaining to loading ranked matches.
 * 
 * @author ShinyDialga
 *
 */
public class RankedListener implements Listener {

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerJoin(PlayerJoinEvent event) {
		StratusAPIRanked.get().getCurrentlyDisconnected().remove(event.getPlayer().getUniqueId());
		StratusAPIRanked.get().getDisconnectUsernames().remove(event.getPlayer().getUniqueId());
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onPlayerQuit(PlayerQuitEvent event) {
		if (StratusAPIRanked.get().getCurrentMatch() != null &&
				StratusAPIPGM.get().getParticipantManager().isPlayerRegistered(
						StratusAPIRanked.get().getCurrentMatch(), event.getPlayer().getUniqueId())) {
			StratusAPIRanked.get().getCurrentlyDisconnected().add(event.getPlayer().getUniqueId());
			StratusAPIRanked.get().getDisconnectUsernames().put(event.getPlayer().getUniqueId(), event.getPlayer().getName());
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onMatchStart(MatchStartEvent event) {
		if (StratusAPIRanked.get().getCurrentMatch() != null) {
			StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().setRanked(true);
			StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().setRankedDiscord(StratusAPIRanked.get().getRankedDiscord());
			StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().setRankedId(StratusAPIRanked.get().getCurrentRankedMatch().getRankedId());

			StratusAPIPGM.get().getStatisticsManager().getMatchStatistics().setRankedPremium(StratusAPIRanked.get().getCurrentRankedMatch().isPremium());
			RankedMatch match = StratusAPIRanked.get().getCurrentRankedMatch();
			for (RankedPlayer rankedPlayer : match.getTeam1()) {
				UUID uuid = rankedPlayer.getUuid();
				Player player = Bukkit.getPlayer(uuid);
				if (player == null || !player.isOnline()) {
					StratusAPIRanked.get().getCurrentlyDisconnected().add(uuid);
					if (player != null)
						StratusAPIRanked.get().getDisconnectUsernames().put(uuid, player.getName());
				}
			}
			for (RankedPlayer rankedPlayer : match.getTeam2()) {
				UUID uuid = rankedPlayer.getUuid();
				Player player = Bukkit.getPlayer(uuid);
				if (player == null || !player.isOnline()) {
					StratusAPIRanked.get().getCurrentlyDisconnected().add(uuid);
					if (player != null)
						StratusAPIRanked.get().getDisconnectUsernames().put(uuid, player.getName());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.LOW)
	public void onMatchLoad(MatchLoadEvent event) {
		StratusAPIPGM.get().getReadyManager().newMatch(event.getMatch());
		StratusAPIPGM.get().getParticipantManager().newMatch(event.getMatch());

		//Only runs on the very first match if nobody has joined the server yet
		if (!StratusAPIRanked.get().isReady()) {
			StratusAPIRanked.get().setReady(true);
		    if (StratusAPIRanked.get().getCurrentRankedMatch() != null) {
                PGM.get().getMapOrder().setNextMap(PGM.get().getMapLibrary().getMap(StratusAPIRanked.get().getCurrentRankedMatch().getMap()));
                Match currentMatch = event.getMatch();
                currentMatch.needModule(CycleMatchModule.class).startCountdown(Duration.ofSeconds(5));
				return;
            }
		}

		Match match = event.getMatch();
		if (StratusAPIRanked.get().getCurrentRankedMatch() != null) {
			RankedMatch rankedMatch = StratusAPIRanked.get().getCurrentRankedMatch();
			StratusAPIRanked.get().setCurrentMatch(match);

			TeamMatchModule teamMatchModule = match.getModule(TeamMatchModule.class);
			if (teamMatchModule == null) {
				return;
			}

			Iterator<Team> teams = teamMatchModule.getTeams().iterator();
			Team team1 = null;
            Team team2 = null;
			while (teams.hasNext()) {
				Team next = teams.next();
				if (next.getShortName().contains("Red")) {
					team1 = next;
				}
				if (next.getShortName().contains("Blue")) {
					team2 = next;
				}
			}

			if (team1 != null && team2 != null) {
				for (RankedPlayer rankedPlayer : rankedMatch.getTeam1()) {
					StratusAPIPGM.get().getParticipantManager().registerParticipant(match, rankedPlayer.getUuid(), team1);

					Player player = Bukkit.getPlayer(rankedPlayer.getUuid());
					if (player != null && player.isOnline()) {
						SingleAudience audience = new SingleAudience(player);
						audience.sendMessage("ranked.punishmentreminder.1");
						audience.sendMessage("ranked.punishmentreminder.2");
					}
				}
				for (RankedPlayer rankedPlayer : rankedMatch.getTeam2()) {
					StratusAPIPGM.get().getParticipantManager().registerParticipant(match, rankedPlayer.getUuid(), team2);
					Player player = Bukkit.getPlayer(rankedPlayer.getUuid());
					if (player != null && player.isOnline()) {
						SingleAudience audience = new SingleAudience(player);
						audience.sendMessage("ranked.punishmentreminder.1");
						audience.sendMessage("ranked.punishmentreminder.2");
					}
				}
			}

			for (RankedPlayer rankedPlayer : rankedMatch.getTeam1()) {
				UUID uuid = rankedPlayer.getUuid();
				Player player = Bukkit.getPlayer(uuid);
				if (player == null || !player.isOnline()) {
					StratusAPIRanked.get().getCurrentlyDisconnected().add(uuid);
					if (player != null)
						StratusAPIRanked.get().getDisconnectUsernames().put(uuid, player.getName());
				}
			}
			for (RankedPlayer rankedPlayer : rankedMatch.getTeam2()) {
				UUID uuid = rankedPlayer.getUuid();
				Player player = Bukkit.getPlayer(uuid);
				if (player == null || !player.isOnline()) {
					StratusAPIRanked.get().getCurrentlyDisconnected().add(uuid);
					if (player != null)
						StratusAPIRanked.get().getDisconnectUsernames().put(uuid, player.getName());
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR)
	public void onMatchFinish(MatchFinishEvent event) {
		try {
			StratusAPIRanked.get().getMatchFinishPublisher().publish(new MatchFinish(StratusAPIRanked.get().getServerName(), StratusAPIRanked.get().getCurrentRankedMatch().getRankedId()));
		} catch (IOException e) {
			e.printStackTrace();
		}

		StratusAPIRanked.get().getServer().getScheduler().runTaskLater(StratusAPIRanked.get(), () -> {
			StratusAPIRanked.get().getDisconnectTime().clear();
			StratusAPIRanked.get().getCurrentlyDisconnected().clear();
			StratusAPIRanked.get().getForfeits().clear();
			StratusAPIRanked.get().getDisconnectUsernames().clear();
			StratusAPIRanked.get().setCurrentRankedMatch(null);
			StratusAPIRanked.get().setCurrentMatch(null);
		}, 20L * 30L);

	}

}
