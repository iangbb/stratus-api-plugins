/*
 *  Stratus API Plugins
 *  Copyright (C) 2019-2021 Stratus Network
 *
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Affero General Public License as published
 *  by the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Affero General Public License for more details.
 *
 *  You should have received a copy of the GNU Affero General Public License
 *  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package network.stratus.api.ranked.commands;

import app.ashcon.intake.Command;
import app.ashcon.intake.bukkit.parametric.annotation.Sender;
import network.stratus.api.bukkit.chat.MultiAudience;
import network.stratus.api.bukkit.chat.SingleAudience;
import network.stratus.api.pgm.StratusAPIPGM;
import network.stratus.api.pgm.match.ReadyManager;
import network.stratus.api.ranked.StratusAPIRanked;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tc.oc.pgm.api.PGM;
import tc.oc.pgm.api.match.Match;
import tc.oc.pgm.api.match.MatchPhase;
import tc.oc.pgm.api.party.Party;
import tc.oc.pgm.api.player.MatchPlayer;
import tc.oc.pgm.start.StartCountdown;
import tc.oc.pgm.start.StartMatchModule;
import tc.oc.pgm.util.named.NameStyle;

import java.time.Duration;
import java.util.stream.Collectors;

/**
 * Commands for readying and unreadying teams.
 * 
 * @author Ian Ballingall
 *
 */
public class ReadyCommands {

	@Command(aliases = "ready",
			desc = "Mark your team as ready for the match")
	public void onReady(@Sender CommandSender sender) {
		ReadyManager manager = StratusAPIPGM.get().getReadyManager();
		MatchPlayer player = PGM.get().getMatchManager().getPlayer((Player) sender);
		Match match = player.getMatch();
		if (match.getPhase() != MatchPhase.IDLE && match.getPhase() != MatchPhase.STARTING) {
			new SingleAudience(sender).sendMessage("team.ready.matchhasstarted");
			return;
		}

		if (StratusAPIRanked.get().getCurrentRankedMatch() == null) {
			new SingleAudience(sender).sendMessage("team.ready.nomatch");
			return;
		}

		if (!player.getId().equals(StratusAPIRanked.get().getCurrentRankedMatch().getTeam1().get(0).getUuid()) &&
				!player.getId().equals(StratusAPIRanked.get().getCurrentRankedMatch().getTeam2().get(0).getUuid())) {
			new SingleAudience(sender).sendMessage("team.ready.notcaptain");
			return;
		}

		Party party = player.getParty();
		boolean changed = manager.setReadyStatus(party, true);

		if (changed) {
			new MultiAudience.Builder()
					.addTargets(match.getPlayers().stream().map(MatchPlayer::getBukkit).collect(Collectors.toList()))
					.build().sendMessage("team.ready", party.getColor() + party.getNameLegacy());

			if (manager.areAllTeamsReady(match)) {
				match.needModule(StartMatchModule.class).forceStartCountdown(Duration.ofSeconds(5), Duration.ZERO);
			}
		} else {
			new SingleAudience(sender).sendMessage("team.ready.already");
		}
	}

//	@Command(aliases = "unready",
//			desc = "Mark your team as not ready for the match")
//	public void onUnready(@Sender CommandSender sender) {
//		MatchPlayer player = PGM.get().getMatchManager().getPlayer((Player) sender);
//		Match match = player.getMatch();
//		if (match.getPhase() != MatchPhase.IDLE && match.getPhase() != MatchPhase.STARTING) {
//			new SingleAudience(sender).sendMessage("team.ready.matchhasstarted");
//			return;
//		}
//
//		if (StratusAPIRanked.get().getCurrentRankedMatch() == null) {
//			return;
//		}
//
//		if (!player.getId().toString().equals(StratusAPIRanked.get().getCurrentRankedMatch().getTeam1().get(0)) &&
//				!player.getId().toString().equals(StratusAPIRanked.get().getCurrentRankedMatch().getTeam2().get(0))) {
//			return;
//		}
//
//		Party party = player.getParty();
//		boolean changed = StratusAPIPGM.get().getReadyManager().setReadyStatus(party, false);
//
//		if (changed) {
//			new MultiAudience.Builder()
//					.addTargets(match.getPlayers().stream().map(MatchPlayer::getBukkit).collect(Collectors.toList()))
//					.build().sendMessage("team.unready", party.getName(NameStyle.COLOR));
//
//			match.getCountdown().cancelAll(StartCountdown.class);
//		} else {
//			new SingleAudience(sender).sendMessage("team.unready.already");
//		}
//
//	}

}
